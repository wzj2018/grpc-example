package main

import (
	weather "another/pb"
	"context"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func run(IP  string) error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	gwmux := runtime.NewServeMux()

	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithAuthority("a"))

	//*****************以下是需要修改的地方********************
	//每新建一个rpc service,就要在这里给新的service注册一下
	//这里事例了三个service的注册：ScannerService, MetalBathService, SealerService(名字为对应proto中的service name)
	//make命令生成的注册函数命名方式固定为：RegisterXXXXXXXHandlerFromEndpoint(...)

	//err := pb.RegisterScannerServiceHandlerFromEndpoint(ctx, gwmux, IP, opts)
	//if err != nil {
	//	return err
	//}
	//
	//err = pb.RegisterMetalBathServiceHandlerFromEndpoint(ctx, gwmux, IP, opts)
	//if err != nil {
	//	return err
	//}
	//
	//err = pb.RegisterSealerServiceHandlerFromEndpoint(ctx, gwmux, IP, opts)
	//if err != nil {
	//	return err
	//}

	err := weather.RegisterWeatherHandlerFromEndpoint(ctx, gwmux, IP, opts)
	if err != nil {
		return err
	}

	//示例:
	//	err = pb.RegisterNewXXXServiceHandlerFromEndpoint(ctx, gwmux, IP, opts)
	//	if err != nil {
	//		return err
	//	}

	//****************以上是需要修改的地方***********************

	mux := http.NewServeMux()
	mux.Handle("/", gwmux)

	bytes, _ := ioutil.ReadFile("./swagger/favicon.ico")

	mux.Handle("/swagger/", http.FileServer(http.Dir(".")))
	mux.HandleFunc("/favicon.ico", func(writer http.ResponseWriter, request *http.Request) {
		_, err = writer.Write(bytes)
		if err != nil {
			_, _ = writer.Write([]byte(err.Error()))
		}
	})

	//指定gateway swagger的监听端口，目前是localhost:8888
	log.Println("Gateway started, visit localhost:8888/swagger in your Chrome browser.")
	return http.ListenAndServe(":8888", mux)
}

func main() {
	log.SetFlags(log.Lshortfile|log.Ltime)
	if err := run(os.Args[1]); err != nil {
		log.Fatal(err)
	}
}
