//tab标签实现
window.onload = function () {
    var title = document.getElementById("Navigation-item").getElementsByTagName("li");
    var page = document.getElementById("Operation-item").getElementsByTagName("li");
    for (var i = 0, len = title.length; i < len; i++) {
        title[i].id = i;
        title[i].onclick = function () {
            for (j = 0; j < len; j++) {
                title[j].classList.remove("selected");
                page[j].classList.remove("selected");

            }
            this.className = "selected";
            page[this.id].className = "selected";

        }

    }


};

new Vue({
    el: "#app",
    data: {
        url: "http://10.6.3.74:8888",
        openLid: {
            Y: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0,
            },
            jaw: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            Z: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            Echo: {
                flag: false,
                Step: ""
            }
        },
        ScanCode: {
            position: 0,
            speed: 0,
            acc: 0,
            dec: 0

        },
        XR: {
            Y: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0,
            },
            jaw: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            Z: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            rotate: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
        },
        DP8: {
            X: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            Y: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            Z: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            suck: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            }


        },
        Seal: {

            Z: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            }


        },
        DP1: {
            Jaw: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            JawZ: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            X: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            Y: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },

        },
        DJ: {
            X: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            Y: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            Z: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            Pump: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },

        },
        QPCR: {
            Filter: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            BathX: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            BathZ: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
        },
        Logistics: {
            X: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            JawY: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            JawZ: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            Jaw: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },

        },
        InitSet: {
            pos: 0,
            speed: 8000,
            acc: 10000,
            dec: 10000,

        },
        DP8XYZ: {
            flag: false,
            POS1: {
                "dp8": {
                    "loc_horizen": {
                        "electl": [
                            {
                                //X轴运动
                                "id": 1,
                                "pre_wait": 0,
                                "ele": "dp8_x",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //Y轴运动
                                "id": 2,
                                "pre_wait": 0,
                                "ele": "dp8_y",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //Z轴运动安全位置
                                "id": 3,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "loc_vertical": {
                        "electl": [
                            {
                                //z轴运动
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //xyz联动时吸液电机位置
                                "id": 2,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "probe": {
                        "electl": {
                            //液位探测极限位
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "suck_act": {
                        "electl": [
                            {
                                //吸液z轴相对位移
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //吸液电机相对位移
                                "id": 2,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "suck_end": {
                        "electl": {
                            //吸液完成z位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "droptip_down": {
                        "electl": {
                            //退tip头吸液电机下位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "droptip_up": {
                        "electl": {
                            //退tip头吸液电机上位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "spit_down": {
                        "electl": {
                            //喷液吸液电机下位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "spit_up": {
                        "electl": {
                            //喷液吸液电机上位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "ontip_down": {
                        "electl": {
                            //上tip头z轴下位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "ontip_up": {
                        "electl": {
                            //上tip头z轴上位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    }
                }
            },
            POS2: {
                "dp8": {
                    "loc_horizen": {
                        "electl": [
                            {
                                //X轴运动
                                "id": 1,
                                "pre_wait": 0,
                                "ele": "dp8_x",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //Y轴运动
                                "id": 2,
                                "pre_wait": 0,
                                "ele": "dp8_y",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //Z轴运动安全位置
                                "id": 3,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "loc_vertical": {
                        "electl": [
                            {
                                //z轴运动
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //xyz联动时吸液电机位置
                                "id": 2,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "probe": {
                        "electl": {
                            //液位探测极限位
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "suck_act": {
                        "electl": [
                            {
                                //吸液z轴相对位移
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //吸液电机相对位移
                                "id": 2,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "suck_end": {
                        "electl": {
                            //吸液完成z位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "droptip_down": {
                        "electl": {
                            //退tip头吸液电机下位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "droptip_up": {
                        "electl": {
                            //退tip头吸液电机上位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "spit_down": {
                        "electl": {
                            //喷液吸液电机下位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "spit_up": {
                        "electl": {
                            //喷液吸液电机上位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "ontip_down": {
                        "electl": {
                            //上tip头z轴下位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "ontip_up": {
                        "electl": {
                            //上tip头z轴上位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    }
                }

            }


        },
        config: {
            //OpenLid   ID=0
            //CloseLid  ID=1
            //xr        ID=2
            //Bash      ID=3
            //Trans     ID=4
            //dp8       ID=5
            //Index     ID=6
            OpenLid: {},
            CloseLid: {},
            xr: {
                "xr": {

                    "gotoStartY": {
                        //Y轴移动位置
                        "electl": {
                            "id": 1,
                            "ele": "xr_y",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "startRotate": {
                        //旋转电机运动位置
                        "electl": {
                            "id": 1,
                            "ele": "xr_r",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "startDown": {
                        //z轴开始位置
                        "electl": {
                            "id": 1,
                            "ele": "xr_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "jawclose": {
                        //夹爪关闭参数
                        "electl": {
                            "id": 1,
                            "ele": "xr_jaw",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "rotate": {
                        //夹取后z轴安全位置
                        "electl": [{
                            "id": 1,
                            "ele": "xr_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        },
                            {
                                //夹取后旋转电机目标
                                "id": 1,
                                "pre_wait": 0,
                                "ele": "xr_r",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "gotoTarY": {
                        "electl": [{
                            //Y轴目标位置
                            "id": 1,
                            "ele": "xr_y",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        },
                            {
                                "id": 2,
                                "ele": "xr_r",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]

                    },
                    "downTar": {
                        "electl": {
                            //z轴目标位置
                            "id": 1,
                            "ele": "xr_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "jawopen": {
                        //夹爪打开参数
                        "electl": [{
                            "id": 1,
                            "ele": "xr_jaw",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        },
                            {

                                "id": 2,
                                "pre_wait": 0,
                                "ele": "xr_y",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "goreset": {
                        "electl": [{
                            //完成后z轴位置
                            "id": 1,
                            "ele": "xr_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        },
                            {
                                //完成后y轴位置
                                "id": 2,
                                "pre_wait": 0,
                                "ele": "xr_y",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    }
                }
            },
            Bash: {},
            Trans: {},
            dp8: {
                "dp8": {
                    "loc_horizen": {
                        "electl": [
                            {
                                //X轴运动
                                "id": 1,
                                "pre_wait": 0,
                                "ele": "dp8_x",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //Y轴运动
                                "id": 2,
                                "pre_wait": 0,
                                "ele": "dp8_y",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //Z轴运动安全位置
                                "id": 3,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "loc_vertical": {
                        "electl": [
                            {
                                //z轴运动
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //xyz联动时吸液电机位置
                                "id": 2,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "probe": {
                        "electl": {
                            //液位探测极限位
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "suck_act": {
                        "electl": [
                            {
                                //吸液z轴相对位移
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //吸液电机相对位移
                                "id": 2,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "suck_end": {
                        "electl": {
                            //吸液完成z位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "droptip_down": {
                        "electl": {
                            //退tip头吸液电机下位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "droptip_up": {
                        "electl": {
                            //退tip头吸液电机上位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "spit_down": {
                        "electl": {
                            //喷液吸液电机下位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "spit_up": {
                        "electl": {
                            //喷液吸液电机上位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "ontip_down": {
                        "electl": {
                            //上tip头z轴下位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "ontip_up": {
                        "electl": {
                            //上tip头z轴上位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    }
                }
            },
            heat_seal: {
                "heat_seal": {
                    "gotosafe": {
                        "electl": [{
                            "id": 1,
                            "ele": "xr_z",
                            "speed": 4000,
                            "acc": 4000,
                            "dec": 4000,
                            "position": 0
                        },
                            {
                                "id": 1,
                                "ele": "heat_z",
                                "speed": 4000,
                                "acc": 4000,
                                "dec": 4000,
                                "position": 0
                            }
                        ]
                    },
                    "xr_rotate": {
                        "electl": {
                            "id": 1,
                            "ele": "xr_r",
                            "speed": 500,
                            "acc": 4000,
                            "dec": 4000,
                            "position": 1530
                        }
                    },
                    "gotoheat": {
                        "electl": {
                            "id": 1,
                            "ele": "xr_y",
                            "speed": 4000,
                            "acc": 4000,
                            "dec": 4000,
                            "position": 5000
                        }
                    },
                    "heatdown": {
                        "electl": {
                            "id": 1,
                            "ele": "heat_z",
                            "speed": 4000,
                            "acc": 4000,
                            "dec": 4000,
                            "position": 2000
                        }
                    },
                    "heatup": {
                        "electl": {
                            "id": 1,
                            "pre_wait": 5000,
                            "ele": "heat_z",
                            "speed": 4000,
                            "acc": 4000,
                            "dec": 4000,
                            "position": 2000
                        }
                    },
                    "goreset": {
                        "electl": {
                            "id": 1,
                            "ele": "xr_y",
                            "speed": 4000,
                            "acc": 4000,
                            "dec": 4000,
                            "position": 2000
                        }
                    },
                    "rotate_reset": {
                        "electl": {
                            "id": 1,
                            "ele": "xr_r",
                            "speed": 250,
                            "acc": 4000,
                            "dec": 4000,
                            "position": 0
                        }
                    }

                }
            }
        },
        num: 0,
        AtoBFinalEcho: {
            flag: false,
        },
        AtoBEcho: {
            flag: false,
        },
        DP8XYZTar: {
            tarNum: 0,
            config: {
                "dp8": {
                    "loc_horizen": {
                        "electl": [
                            {
                                //X轴运动
                                "id": 1,
                                "pre_wait": 0,
                                "ele": "dp8_x",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //Y轴运动
                                "id": 2,
                                "pre_wait": 0,
                                "ele": "dp8_y",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //Z轴运动安全位置
                                "id": 3,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "loc_vertical": {
                        "electl": [
                            {
                                //z轴运动
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //xyz联动时吸液电机位置
                                "id": 2,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "probe": {
                        "electl": {
                            //液位探测极限位
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "suck_act": {
                        "electl": [
                            {
                                //吸液z轴相对位移
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //吸液电机相对位移
                                "id": 2,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "suck_end": {
                        "electl": {
                            //吸液完成z位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "droptip_down": {
                        "electl": {
                            //退tip头吸液电机下位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "droptip_up": {
                        "electl": {
                            //退tip头吸液电机上位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "spit_down": {
                        "electl": {
                            //喷液吸液电机下位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "spit_up": {
                        "electl": {
                            //喷液吸液电机上位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "ontip_down": {
                        "electl": {
                            //上tip头z轴下位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "ontip_up": {
                        "electl": {
                            //上tip头z轴上位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    }
                }
            },
            tarXYZ: {
                1: {
                    x: 5561,
                    y: 21842,
                    z: 126000,
                },
                2: {
                    x: 6164,
                    y: 21842,
                    z: 126000,
                },
                3: {
                    x: 6767,
                    y: 21842,
                    z: 126000,
                },
                4: {
                    x: 7370,
                    y: 21842,
                    z: 126000,
                },
                5: {
                    x: 7973,
                    y: 21842,
                    z: 126000,
                },
                6: {
                    x: 5628,
                    y: 28140,
                    z: 0,
                },
                7: {
                    x: 6231,
                    y: 28140,
                    z: 0,
                },
                8: {
                    x: 6834,
                    y: 28140,
                    z: 0,
                },
                9: {
                    x: 7437,
                    y: 28140,
                    z: 0,
                },
                10: {
                    x: 8643,
                    y: 28140,
                    z: 0,
                },
                11: {
                    x: 12261,
                    y: 28140,
                    z: 0,
                },
                12: {
                    x: 25125,
                    y: 21976,
                    z: 32000,
                },
                13: {
                    x: 536,
                    y: 21373,
                    z: 37800,
                },
                14: {
                    x: 536,
                    y: 28274,
                    z: 37800,
                },
                15: {
                    x: 9849,
                    y: 28140,
                    z: 32600,
                },
                16: {
                    x: 10452,
                    y: 28140,
                    z: 32600,
                },
                17: {
                    x: 11055,
                    y: 28140,
                    z: 32600,
                },


            },

        },
        DP8XYZMove: {
            x: 0,
            y: 0,
            z: 0,
            config: {
                "dp8": {
                    "loc_horizen": {
                        "electl": [
                            {
                                //X轴运动
                                "id": 1,
                                "pre_wait": 0,
                                "ele": "dp8_x",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //Y轴运动
                                "id": 2,
                                "pre_wait": 0,
                                "ele": "dp8_y",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //Z轴运动安全位置
                                "id": 3,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "loc_vertical": {
                        "electl": [
                            {
                                //z轴运动
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //xyz联动时吸液电机位置
                                "id": 2,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "probe": {
                        "electl": {
                            //液位探测极限位
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "suck_act": {
                        "electl": [
                            {
                                //吸液z轴相对位移
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //吸液电机相对位移
                                "id": 2,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "suck_end": {
                        "electl": {
                            //吸液完成z位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "droptip_down": {
                        "electl": {
                            //退tip头吸液电机下位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "droptip_up": {
                        "electl": {
                            //退tip头吸液电机上位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "spit_down": {
                        "electl": {
                            //喷液吸液电机下位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "spit_up": {
                        "electl": {
                            //喷液吸液电机上位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "ontip_down": {
                        "electl": {
                            //上tip头z轴下位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "ontip_up": {
                        "electl": {
                            //上tip头z轴上位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    }
                }
            },
        },
        DP8suck: {
            tarNum: 0,
            config: {
                "dp8": {
                    "loc_horizen": {
                        "electl": [
                            {
                                //X轴运动
                                "id": 1,
                                "pre_wait": 0,
                                "ele": "dp8_x",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //Y轴运动
                                "id": 2,
                                "pre_wait": 0,
                                "ele": "dp8_y",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //Z轴运动安全位置
                                "id": 3,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "loc_vertical": {
                        "electl": [
                            {
                                //z轴运动
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //xyz联动时吸液电机位置
                                "id": 2,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "probe": {
                        "electl": {
                            //液位探测极限位
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "suck_act": {
                        "electl": [
                            {
                                //吸液z轴相对位移
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //吸液电机相对位移
                                "id": 2,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "suck_end": {
                        "electl": {
                            //吸液完成z位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "droptip_down": {
                        "electl": {
                            //退tip头吸液电机下位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "droptip_up": {
                        "electl": {
                            //退tip头吸液电机上位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "spit_down": {
                        "electl": {
                            //喷液吸液电机下位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "spit_up": {
                        "electl": {
                            //喷液吸液电机上位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "ontip_down": {
                        "electl": {
                            //上tip头z轴下位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "ontip_up": {
                        "electl": {
                            //上tip头z轴上位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    }
                }
            }
        },
        DP8drop: {
            tarNum: 0,
            config: {
                "dp8": {
                    "loc_horizen": {
                        "electl": [
                            {
                                //X轴运动
                                "id": 1,
                                "pre_wait": 0,
                                "ele": "dp8_x",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //Y轴运动
                                "id": 2,
                                "pre_wait": 0,
                                "ele": "dp8_y",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //Z轴运动安全位置
                                "id": 3,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "loc_vertical": {
                        "electl": [
                            {
                                //z轴运动
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //xyz联动时吸液电机位置
                                "id": 2,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "probe": {
                        "electl": {
                            //液位探测极限位
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "suck_act": {
                        "electl": [
                            {
                                //吸液z轴相对位移
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //吸液电机相对位移
                                "id": 2,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "suck_end": {
                        "electl": {
                            //吸液完成z位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "droptip_down": {
                        "electl": {
                            //退tip头吸液电机下位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "droptip_up": {
                        "electl": {
                            //退tip头吸液电机上位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "spit_down": {
                        "electl": {
                            //喷液吸液电机下位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "spit_up": {
                        "electl": {
                            //喷液吸液电机上位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "ontip_down": {
                        "electl": {
                            //上tip头z轴下位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "ontip_up": {
                        "electl": {
                            //上tip头z轴上位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    }
                }
            },
        },
        DP8TipOn: {
            tarNum: 0,
            config: {
                "dp8": {
                    "loc_horizen": {
                        "electl": [
                            {
                                //X轴运动
                                "id": 1,
                                "pre_wait": 0,
                                "ele": "dp8_x",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //Y轴运动
                                "id": 2,
                                "pre_wait": 0,
                                "ele": "dp8_y",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //Z轴运动安全位置
                                "id": 3,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "loc_vertical": {
                        "electl": [
                            {
                                //z轴运动
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //xyz联动时吸液电机位置
                                "id": 2,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "probe": {
                        "electl": {
                            //液位探测极限位
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "suck_act": {
                        "electl": [
                            {
                                //吸液z轴相对位移
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //吸液电机相对位移
                                "id": 2,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "suck_end": {
                        "electl": {
                            //吸液完成z位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "droptip_down": {
                        "electl": {
                            //退tip头吸液电机下位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "droptip_up": {
                        "electl": {
                            //退tip头吸液电机上位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "spit_down": {
                        "electl": {
                            //喷液吸液电机下位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "spit_up": {
                        "electl": {
                            //喷液吸液电机上位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "ontip_down": {
                        "electl": {
                            //上tip头z轴下位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "ontip_up": {
                        "electl": {
                            //上tip头z轴上位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    }
                }
            },
        },
        DP8TipDown: {
            tarNum: 0,
            config: {
                "dp8": {
                    "loc_horizen": {
                        "electl": [
                            {
                                //X轴运动
                                "id": 1,
                                "pre_wait": 0,
                                "ele": "dp8_x",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //Y轴运动
                                "id": 2,
                                "pre_wait": 0,
                                "ele": "dp8_y",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //Z轴运动安全位置
                                "id": 3,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "loc_vertical": {
                        "electl": [
                            {
                                //z轴运动
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //xyz联动时吸液电机位置
                                "id": 2,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "probe": {
                        "electl": {
                            //液位探测极限位
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "suck_act": {
                        "electl": [
                            {
                                //吸液z轴相对位移
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                            {
                                //吸液电机相对位移
                                "id": 2,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        ]
                    },
                    "suck_end": {
                        "electl": {
                            //吸液完成z位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "droptip_down": {
                        "electl": {
                            //退tip头吸液电机下位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "droptip_up": {
                        "electl": {
                            //退tip头吸液电机上位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "spit_down": {
                        "electl": {
                            //喷液吸液电机下位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "spit_up": {
                        "electl": {
                            //喷液吸液电机上位置
                            "id": 1,
                            "ele": "dp8_suck",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "ontip_down": {
                        "electl": {
                            //上tip头z轴下位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    },
                    "ontip_up": {
                        "electl": {
                            //上tip头z轴上位置
                            "id": 1,
                            "ele": "dp8_z",
                            "speed": 0,
                            "acc": 0,
                            "dec": 0,
                            "position": 0
                        }
                    }
                }
            },
        },
        
    },
    mounted: function () {
        this.getConfig();
    },
    methods: {
        getConfig: function () {
            let _s = this;

            if (_s.num < Object.keys(_s.config).length) {
                axios.post(_s.url + "/v1/param/to_pc", {
                    "fileId": Number(_s.num)
                })
                    .then(function (res) {
                        if (res.data.length != 0) {
                            _s.ConfigProcess(res.data, _s.num);
                        }
                        _s.num++;
                        _s.getConfig();
                    })
            } else {
                console.log(_s.config);
            }

        },
        ConfigProcess: function (ConfigFile, k) {

            var _s = this;
            if (typeof (ConfigFile) == "string") {
                var b = [];
                var result = "";
                var a = ConfigFile.split("{\"result\":");
                var j = 0;
                for (var i = 1; i < a.length; i++) {
                    a[i] = a[i].substring(0, a[i].length - 2);
                    j = i - 1;
                    b[j] = JSON.parse(a[i]);
                }
                for (var i = 0; i < b.length; i++) {
                    result = result + b[i].chunk;
                }
                result = JSON.parse(result);
            } else if (typeof (ConfigFile) == "object") {
                result = JSON.parse(ConfigFile.result.chunk)
            }
            switch (k) {
                case 0:
                    _s.config.OpenLid = result;
            }
            switch (k) {
                case 0:
                    _s.config.OpenLid = result;
                    break;
                case 1:
                    _s.config.CloseLid = result;
                    break;
                case 2:
                    _s.config.xr = result;
                    break;
                case 3:
                    _s.config.Bash = result;
                    break;
                case 4:
                    _s.config.Trans = result;
                    break;
                case 5:
                    _s.config.dp8 = result;
                    break;
                case 6:
                    _s.config.heat_seal = result;
                    break;
                default:
                    alert("配置文件处理出错");
                    break;
            }
            console.log(result, k);
        },
        // saveConfig: function (Filesize, str, num) {
        //     let _s = this;
        //     axios.post(_s.url + "/v1/param/to_board", {
        //         "fileId": Number(num),
        //         "filesize": Filesize,
        //         "chunk": str
        //     })
        //         .then(function (res) {
        //             console.log(res)
        //         })
        // },
        // saveConfigProcess: function (num, tar) {
        //     let _s = this;
        //     var str = JSON.stringify(tar);
        //     console.log(str, str.length);
        //     _s.saveConfig(str.length, str, num)
        // },
        getDP8config: function () {
            this.DP8XYZ.POS1 = this.config.dp8;
            this.DP8XYZ.POS2 = this.config.dp8;
            this.DP8XYZTar.config = this.config.dp8;
            this.DP8XYZMove.config = this.config.dp8;
            this.DP8suck.config = this.config.dp8;
            this.DP8drop.config = this.config.dp8;
            this.DP8TipOn.config = this.config.dp8;
            this.DP8TipDown.config = this.config.dp8;
            console.log("DP8参数设定完成")
        },
        fDP8XYZstart: function () {
            this.DP8XYZ.flag = true;
            this.fDP8XYZ()
        },
        fDP8XYZstop: function () {
            this.DP8XYZ.flag = false;
        },
        fDP8XYZ: function () {
            let _s = this;
            var pos1 = JSON.stringify(_s.DP8XYZ.POS1);
            var pos2 = JSON.stringify(_s.DP8XYZ.POS2);
            if (this.DP8XYZ.flag == true) {
                axios.post(_s.url + "/v1/param/to_board", {
                    "fileId": 5,
                    "filesize": pos1.length,
                    "chunk": pos1
                })
                    .then(function (res) {
                        setTimeout(function () {
                            axios.post(_s.url + "/v1/module/DP8_MOVE")
                                .then(function (res) {
                                    if (res.data.errorNum == 100) {
                                        setTimeout(function () {
                                            axios.post(_s.url + "/v1/param/to_board", {
                                                "fileId": 5,
                                                "filesize": pos2.length,
                                                "chunk": pos2
                                            })
                                                .then(function (res) {
                                                    setTimeout(function () {
                                                        axios.post(_s.url + "/v1/module/DP8_MOVE")
                                                            .then(function (res) {
                                                                if (res.data.errorNum == 100) {
                                                                    setTimeout(function () {
                                                                        _s.fDP8XYZ
                                                                    }, 2000)

                                                                }
                                                            })
                                                    }, 1000)

                                                })
                                        }, 1000)
                                    }
                                })
                                .catch(function (err) {
                                    alert(err);
                                })
                        }, 1000);

                    })
                    .catch(function (err) {
                        alert(err);
                    })
            }

        },

        fAtoBstart: function () {
            this.AtoBEcho.flag = true;
            this.fAtoBEcho();
        },

        fAtoBstop: function () {
            this.AtoBEcho.flag = false;
        },

        fAtoBEcho: function () {
            let _s = this;
            if (_s.AtoBEcho.flag == true) {
                axios.post(_s.url + "/v1/param/to_board", {
                    "fileId": 2,
                    "filesize": 2070,
                    "chunk": "{\"xr\": {\"gotoStartY2\": {\"electl\": [{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0}]},\"gotoStartY\": {\"electl\": {\"id\": 1,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4350}},\"startRotate\": {\"electl\": {\"id\": 1,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 1540}},\"startDown\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 2000,\"acc\": 2000,\"dec\": 2000,\"position\": 2300}},\"jawclose\": {\"electl\": {\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 770}},\"rotate\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 1,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 6350},{\"id\": 1,\"pre_wait\": 2000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4200}]},\"gotoTarY\": {\"electl\": [{\"id\": 1,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4140},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4750}]},\"gotoTarY2\": {\"electl\": [{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4140},{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4750}]},\"downTar\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4100}},\"jawopen\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 0},{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4140}]},\"goreset1\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4200},{\"id\": 2,\"pre_wait\": 2000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 6350}]},\"goreset2\": {\"electl\": [{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0},{\"id\": 2,\"pre_wait\": 10000,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0}]}}}"
                })
                    .then(function (res) {
                        setTimeout(function () {
                            axios.post(_s.url + "/v1/module/XR_AToB", {})
                                .then(function (res) {
                                    if (res.data.errorNum == 100) {
                                        setTimeout(function () {
                                            axios.post(_s.url + "/v1/param/to_board", {
                                                "fileId": 2,
                                                "filesize": 2140,
                                                "chunk": "{\"xr\": {\"gotoStartY2\": {\"electl\": [{\"id\": 2,\"pre_wait\": 200,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 6350},{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4200}]},\"gotoStartY\": {\"electl\": {\"id\": 1,\"pre_wait\": 200,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4140}},\"startRotate\": {\"electl\": {\"id\": 1,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4750}},\"startDown\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 2000,\"acc\": 2000,\"dec\": 2000,\"position\": 4100}},\"jawclose\": {\"electl\": {\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 770}},\"rotate\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 1,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4140},{\"id\": 1,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4200}]},\"gotoTarY\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 6350},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 1540}]},\"gotoTarY2\": {\"electl\": [{\"id\": 2,\"pre_wait\": 200,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4350},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 1540}]},\"downTar\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 2000,\"acc\": 2000,\"dec\": 2000,\"position\": 2300}},\"jawopen\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4350}]},\"goreset1\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4350}]},\"goreset2\": {\"electl\": [{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0}]}}}"
                                            })
                                                .then(function (res) {
                                                    setTimeout(function () {
                                                        axios.post(_s.url + "/v1/module/XR_AToB", {})
                                                            .then(function (res) {
                                                                if (res.data.errorNum == 100) {
                                                                    setTimeout(function () {
                                                                        _s.fAtoBEcho();
                                                                    }, 1000)
                                                                }
                                                            })
                                                    }, 1000)
                                                })
                                                .catch(function (err) {
                                                    alert(err);
                                                })
                                        }, 1000)
                                    }
                                })
                                .catch(function (err) {
                                    alert(err);
                                })
                        }, 1000)
                    })
                    .catch(function (err) {
                        alert(err);
                    })
            }
        },



        fAtoBFinalstart: function () {
            this.AtoBFinalEcho.flag = true;
            this.fAtoBFinalEcho();
        },
        fAtoBFinalstop: function () {
            this.AtoBFinalEcho.flag = false;
        },
        fAtoBFinalEcho: function () {
            let _s = this;
            if (_s.AtoBFinalEcho.flag == true) {
                axios.post(_s.url + "/v1/param/to_board", {
                    "fileId": 2,
                    "filesize": 2070,
                    "chunk": "{\"xr\": {\"gotoStartY2\": {\"electl\": [{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0}]},\"gotoStartY\": {\"electl\": {\"id\": 1,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4350}},\"startRotate\": {\"electl\": {\"id\": 1,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 1540}},\"startDown\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 2000,\"acc\": 2000,\"dec\": 2000,\"position\": 2300}},\"jawclose\": {\"electl\": {\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 770}},\"rotate\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 1,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 6350},{\"id\": 1,\"pre_wait\": 2000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4200}]},\"gotoTarY\": {\"electl\": [{\"id\": 1,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4140},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4750}]},\"gotoTarY2\": {\"electl\": [{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4140},{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4750}]},\"downTar\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4100}},\"jawopen\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 0},{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4140}]},\"goreset1\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4200},{\"id\": 2,\"pre_wait\": 2000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 6350}]},\"goreset2\": {\"electl\": [{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0},{\"id\": 2,\"pre_wait\": 10000,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0}]}}}"
                })
                    .then(function (res) {
                        setTimeout(function () {
                            axios.post(_s.url + "/v1/module/XR_AToB", {})
                                .then(function (res) {
                                    if (res.data.errorNum == 100) {
                                        setTimeout(function () {
                                            axios.post(_s.url + "/v1/param/to_board", {
                                                "fileId": 2,
                                                "filesize": 2140,
                                                "chunk": "{\"xr\": {\"gotoStartY2\": {\"electl\": [{\"id\": 2,\"pre_wait\": 200,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 6350},{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4200}]},\"gotoStartY\": {\"electl\": {\"id\": 1,\"pre_wait\": 200,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4140}},\"startRotate\": {\"electl\": {\"id\": 1,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4750}},\"startDown\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 2000,\"acc\": 2000,\"dec\": 2000,\"position\": 4100}},\"jawclose\": {\"electl\": {\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 770}},\"rotate\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 1,\"pre_wait\": 2000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4140},{\"id\": 1,\"pre_wait\": 2000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4200}]},\"gotoTarY\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 6350},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 1540}]},\"gotoTarY2\": {\"electl\": [{\"id\": 2,\"pre_wait\": 200,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4350},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 1540}]},\"downTar\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 2000,\"acc\": 2000,\"dec\": 2000,\"position\": 2300}},\"jawopen\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4350}]},\"goreset1\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4350}]},\"goreset2\": {\"electl\": [{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0}]}}}"
                                            })
                                                .then(function (res) {
                                                    setTimeout(function () {
                                                        axios.post(_s.url + "/v1/module/XR_AToB", {})
                                                            .then(function (res) {
                                                                if (res.data.errorNum == 100) {
                                                                    setTimeout(function () {
                                                                        axios.post(_s.url + "/v1/param/to_board", {
                                                                            "fileId": 2,
                                                                            "filesize": 1942,
                                                                            "chunk": "{\"xr\": {\"gotoStartY2\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0}]},\"gotoStartY\": {\"electl\": {\"id\": 1,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4350}},\"startRotate\": {\"electl\": {\"id\": 1,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 1540}},\"startDown\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 2000,\"acc\": 2000,\"dec\": 2000,\"position\": 2300}},\"jawclose\": {\"electl\": {\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 770}},\"rotate\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 1,\"pre_wait\": 3000,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 1,\"pre_wait\": 3000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 300}]},\"gotoTarY\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 270,\"acc\": 200,\"dec\": 200,\"position\": 0}]},\"gotoTarY2\": {\"electl\": [{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0}]},\"downTar\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4000}},\"jawopen\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 0},{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 0}]},\"goreset1\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0},{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0}]},\"goreset2\": {\"electl\": [{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0},{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0}]}}}"
                                                                        })
                                                                            .then(function (res) {
                                                                                setTimeout(function () {
                                                                                    axios.post(_s.url + "/v1/module/XR_AToB", {})
                                                                                        .then(function (res) {
                                                                                            if (res.data.errorNum == 100) {
                                                                                                setTimeout(function () {
                                                                                                    axios.post(_s.url + "/v1/param/to_board", {
                                                                                                        "fileId": 6,
                                                                                                        "filesize": 1008,
                                                                                                        "chunk": "{\"heat_seal\": {\"gotosafe\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0}, {\"id\": 1,\"ele\": \"heat_z\",\"speed\": 4000,\"acc\": 4000,\"dec\": 4000,\"position\": 0}]},\"xr_rotate\": {\"electl\": {\"id\": 1,\"ele\": \"xr_r\",\"speed\": 4000,\"acc\": 800,\"dec\": 800,\"position\": 1540}},\"gotoheat\": {\"electl\": {\"id\": 1,\"ele\": \"xr_y\",\"speed\": 16000,\"acc\": 16000,\"dec\": 16000,\"position\": 14155}},\"heatdown\": {\"electl\": {\"id\": 1,\"ele\": \"heat_z\",\"speed\": 8000,\"acc\": 8000,\"dec\": 8000,\"position\": 7000}},\"heatup\": {\"electl\": {\"id\": 1,\"pre_wait\": 2000,\"ele\": \"heat_z\",\"speed\": 8000,\"acc\": 8000,\"dec\": 8000,\"position\": 0}},\"goreset\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_y\",\"speed\": 16000,\"acc\": 16000,\"dec\": 16000,\"position\": 120},{\"id\": 1,\"pre_wait\": 1200,\"ele\": \"xr_r\",\"speed\": 4000,\"acc\": 4000,\"dec\": 4000,\"position\": 120}]},\"rotate_reset\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_r\",\"speed\": 270,\"acc\": 200,\"dec\": 200,\"position\": 0},{\"id\": 1,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0}]}}}"
                                                                                                    })
                                                                                                        .then(function (res) {
                                                                                                            setTimeout(function () {
                                                                                                                axios.post(_s.url + "/v1/module/Heat_Seal", {})
                                                                                                                    .then(function (res) {
                                                                                                                        if (res.data.errorNum == 100) {
                                                                                                                            setTimeout(function () {
                                                                                                                                axios.post(_s.url + "/v1/param/to_board", {
                                                                                                                                    "fileId": 2,
                                                                                                                                    "filesize": 2064,
                                                                                                                                    "chunk": "{\"xr\": {\"gotoStartY2\": {\"electl\": [{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0}]},\"gotoStartY\": {\"electl\": {\"id\": 1,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 0}},\"startRotate\": {\"electl\": {\"id\": 1,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 0}},\"startDown\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 2000,\"acc\": 2000,\"dec\": 2000,\"position\": 4000}},\"jawclose\": {\"electl\": {\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 770}},\"rotate\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 1,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 6350},{\"id\": 1,\"pre_wait\": 3000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4200}]},\"gotoTarY\": {\"electl\": [{\"id\": 1,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4140},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4750}]},\"gotoTarY2\": {\"electl\": [{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4140},{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4750}]},\"downTar\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4100}},\"jawopen\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 0},{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4140}]},\"goreset1\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4200},{\"id\": 2,\"pre_wait\": 2000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 6350}]},\"goreset2\": {\"electl\": [{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0},{\"id\": 2,\"pre_wait\": 10000,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0}]}}}"
                                                                                                                                })
                                                                                                                                    .then(function (res) {
                                                                                                                                        setTimeout(function () {
                                                                                                                                            axios.post(_s.url + "/v1/module/XR_AToB", {})
                                                                                                                                                .then(function (res) {
                                                                                                                                                    if (res.data.errorNum == 100) {
                                                                                                                                                        setTimeout(function () {
                                                                                                                                                            axios.post(_s.url + "/v1/param/to_board", {
                                                                                                                                                                "fileId": 2,
                                                                                                                                                                "filesize": 2140,
                                                                                                                                                                "chunk": "{\"xr\": {\"gotoStartY2\": {\"electl\": [{\"id\": 2,\"pre_wait\": 200,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 6350},{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4200}]},\"gotoStartY\": {\"electl\": {\"id\": 1,\"pre_wait\": 200,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4140}},\"startRotate\": {\"electl\": {\"id\": 1,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4750}},\"startDown\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 2000,\"acc\": 2000,\"dec\": 2000,\"position\": 4100}},\"jawclose\": {\"electl\": {\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 770}},\"rotate\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 1,\"pre_wait\": 2000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4140},{\"id\": 1,\"pre_wait\": 2000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4200}]},\"gotoTarY\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 6350},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 1540}]},\"gotoTarY2\": {\"electl\": [{\"id\": 2,\"pre_wait\": 200,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4350},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 1540}]},\"downTar\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 2000,\"acc\": 2000,\"dec\": 2000,\"position\": 2300}},\"jawopen\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4350}]},\"goreset1\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4350}]},\"goreset2\": {\"electl\": [{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0}]}}}"
                                                                                                                                                            })
                                                                                                                                                                .then(function (res) {
                                                                                                                                                                    setTimeout(function () {
                                                                                                                                                                        axios.post(_s.url + "/v1/module/XR_AToB", {})
                                                                                                                                                                            .then(function (res) {
                                                                                                                                                                                if (res.data.errorNum == 100) {
                                                                                                                                                                                    _s.fAtoBFinalEcho()
                                                                                                                                                                                }
                                                                                                                                                                            })
                                                                                                                                                                            .catch(function (err) {
                                                                                                                                                                                alert(err);
                                                                                                                                                                            })
                                                                                                                                                                    }, 1000)
                                                                                                                                                                })
                                                                                                                                                                .catch(function (err) {
                                                                                                                                                                    alert(err);
                                                                                                                                                                })
                                                                                                                                                        }, 1000)
                                                                                                                                                        //      _s.fAtoBFinalEcho()
                                                                                                                                                    }
                                                                                                                                                })
                                                                                                                                                .catch(function (err) {
                                                                                                                                                    alert(err);
                                                                                                                                                })
                                                                                                                                        }, 1000)
                                                                                                                                    })
                                                                                                                                    .catch(function (err) {
                                                                                                                                        alert(err);
                                                                                                                                    })
                                                                                                                            }, 1000)
                                                                                                                        }
                                                                                                                    })
                                                                                                                    .catch(function (err) {
                                                                                                                        alert(err)
                                                                                                                    })
                                                                                                            }, 1000)
                                                                                                        })
                                                                                                        .catch(function (err) {
                                                                                                            alert(err)
                                                                                                        })
                                                                                                }, 1000)
                                                                                            }
                                                                                        })
                                                                                }, 1000)
                                                                            })
                                                                            .catch(function (err) {
                                                                                alert(err);
                                                                            })
                                                                    }, 1000)
                                                                }
                                                            })
                                                    }, 1000)
                                                })
                                                .catch(function (err) {
                                                    alert(err);
                                                })
                                        }, 1000)
                                    }
                                })
                                .catch(function (err) {
                                    alert(err);
                                })
                        }, 1000)
                    })
                    .catch(function (err) {
                        alert(err);
                    })
            }
        },



 //发送空板夹取到DJ位参数
        XRktoDJposi:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 2,
                "filesize": 2116,
                "chunk": "{\"xr\": {\"gotoStartY2\": {\"electl\": [{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0}]},\"gotoStartY\": {\"electl\": {\"id\": 1,\"ele\": \"xr_y\",\"speed\": 16000,\"acc\": 16000,\"dec\": 16000,\"position\": 4350}},\"startRotate\": {\"electl\": {\"id\": 1,\"pre_wait\": 2000,\"ele\": \"xr_r\",\"speed\": 4000,\"acc\": 4000,\"dec\": 4000,\"position\": 1540}},\"startDown\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 8000,\"acc\": 8000,\"dec\": 8000,\"position\": 2300}},\"jawclose\": {\"electl\": {\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 770}},\"rotate\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 2000,\"acc\": 8000,\"dec\": 8000,\"position\": 0},{\"id\": 1,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 16000,\"acc\": 16000,\"dec\": 16000,\"position\": 6350},{\"id\": 1,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 4000,\"acc\": 4000,\"dec\": 4000,\"position\": 4200}]},\"gotoTarY\": {\"electl\": [{\"id\": 1,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 16000,\"acc\": 16000,\"dec\": 16000,\"position\": 4140},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 4000,\"acc\": 4000,\"dec\": 4000,\"position\": 4750}]},\"gotoTarY2\": {\"electl\": [{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 16000,\"acc\": 16000,\"dec\": 16000,\"position\": 4140},{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 4000,\"acc\": 4000,\"dec\": 4000,\"position\": 4750}]},\"downTar\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 8000,\"acc\": 8000,\"dec\": 8000,\"position\": 4100}},\"jawopen\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 0},{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 16000,\"acc\": 16000,\"dec\": 16000,\"position\": 4140}]},\"goreset1\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 2000,\"acc\": 8000,\"dec\": 8000,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 4000,\"acc\": 4000,\"dec\": 4000,\"position\": 120},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 16000,\"acc\": 16000,\"dec\": 16000,\"position\": 6350}]},\"goreset2\": {\"electl\": [{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0},{\"id\": 2,\"pre_wait\": 2000,\"ele\": \"xr_y\",\"speed\": 1500,\"acc\": 4000,\"dec\": 1000,\"position\": 0}]}}}"
            })
                .then(function(response){
                    console.log("设置空板夹取到DJ位参数成功")
                })
                .catch(function(err){
                    alert(err);
                })
        },

 //发送DJ位夹取到空板位参数
        XRDJtokposi:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 2,
                "filesize": 2166,
                "chunk": "{\"xr\": {\"gotoStartY2\": {\"electl\": [{\"id\": 1,\"pre_wait\": 200,\"ele\": \"xr_y\",\"speed\": 16000,\"acc\": 16000,\"dec\": 16000,\"position\": 6350},{\"id\": 2,\"pre_wait\": 2000,\"ele\": \"xr_r\",\"speed\": 4000,\"acc\": 4000,\"dec\": 4000,\"position\": 4200}]},\"gotoStartY\": {\"electl\": {\"id\": 1,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 16000,\"acc\": 16000,\"dec\": 16000,\"position\": 4140}},\"startRotate\": {\"electl\": {\"id\": 1,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4750}},\"startDown\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 2000,\"acc\": 2000,\"dec\": 2000,\"position\": 4100}},\"jawclose\": {\"electl\": {\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 770}},\"rotate\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 1,\"pre_wait\": 2000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4140},{\"id\": 1,\"pre_wait\": 2000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4200}]},\"gotoTarY\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 6350},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 1540}]},\"gotoTarY2\": {\"electl\": [{\"id\": 2,\"pre_wait\": 200,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4350},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 1540}]},\"downTar\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 2000,\"acc\": 2000,\"dec\": 2000,\"position\": 2300}},\"jawopen\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4350}]},\"goreset1\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 2000,\"acc\": 8000,\"dec\": 8000,\"position\": 0},{\"id\": 2,\"pre_wait\": 2000,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4350}]},\"goreset2\": {\"electl\": [{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0}]}}}"
            })
                .then(function(response){
                    console.log("设置DJ位夹取到空板位参数成功")
                })
                .catch(function(err){
                    alert(err);
                })
        },

 //发送空板夹取到热封闲置位参数
        XRktoHeatposi:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 2,
                "filesize": 1947,
                "chunk": "{\"xr\": {\"gotoStartY2\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0}]},\"gotoStartY\": {\"electl\": {\"id\": 1,\"ele\": \"xr_y\",\"speed\": 16000,\"acc\": 16000,\"dec\": 16000,\"position\": 4350}},\"startRotate\": {\"electl\": {\"id\": 1,\"ele\": \"xr_r\",\"speed\": 4000,\"acc\": 4000,\"dec\": 4000,\"position\": 1540}},\"startDown\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 2000,\"acc\": 2000,\"dec\": 2000,\"position\": 2300}},\"jawclose\": {\"electl\": {\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 770}},\"rotate\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 1,\"pre_wait\": 3000,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 1,\"pre_wait\": 3000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 300}]},\"gotoTarY\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 270,\"acc\": 200,\"dec\": 200,\"position\": 0}]},\"gotoTarY2\": {\"electl\": [{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0}]},\"downTar\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4000}},\"jawopen\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 0},{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 0}]},\"goreset1\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0},{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0}]},\"goreset2\": {\"electl\": [{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0},{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0}]}}}"
            })
                .then(function(response){
                    console.log("设置空板夹取到热封闲置位参数成功")
                })
                .catch(function(err){
                    alert(err);
                })
        },

  //发送热封参数
        XRHeatposi:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 6,
                "filesize": 1008,
                "chunk": "{\"heat_seal\": {\"gotosafe\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0}, {\"id\": 1,\"ele\": \"heat_z\",\"speed\": 4000,\"acc\": 4000,\"dec\": 4000,\"position\": 0}]},\"xr_rotate\": {\"electl\": {\"id\": 1,\"ele\": \"xr_r\",\"speed\": 4000,\"acc\": 800,\"dec\": 800,\"position\": 1540}},\"gotoheat\": {\"electl\": {\"id\": 1,\"ele\": \"xr_y\",\"speed\": 16000,\"acc\": 16000,\"dec\": 16000,\"position\": 14155}},\"heatdown\": {\"electl\": {\"id\": 1,\"ele\": \"heat_z\",\"speed\": 8000,\"acc\": 8000,\"dec\": 8000,\"position\": 7000}},\"heatup\": {\"electl\": {\"id\": 1,\"pre_wait\": 2000,\"ele\": \"heat_z\",\"speed\": 8000,\"acc\": 8000,\"dec\": 8000,\"position\": 0}},\"goreset\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_y\",\"speed\": 16000,\"acc\": 16000,\"dec\": 16000,\"position\": 120},{\"id\": 1,\"pre_wait\": 1200,\"ele\": \"xr_r\",\"speed\": 4000,\"acc\": 4000,\"dec\": 4000,\"position\": 120}]},\"rotate_reset\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_r\",\"speed\": 270,\"acc\": 200,\"dec\": 200,\"position\": 0},{\"id\": 1,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0}]}}}"
            })
                .then(function(response){
                    console.log("设置热封参数成功")
                })
                .catch(function(err){
                    alert(err);
                })
        },

 //发送热封闲置位夹取到DJ位参数
        XRHeattoDJposi:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 2,
                "filesize": 2072,
                "chunk": "{\"xr\": {\"gotoStartY2\": {\"electl\": [{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 0},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0}]},\"gotoStartY\": {\"electl\": {\"id\": 1,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 0}},\"startRotate\": {\"electl\": {\"id\": 1,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 0}},\"startDown\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 8000,\"acc\": 8000,\"dec\": 8000,\"position\": 4000}},\"jawclose\": {\"electl\": {\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 770}},\"rotate\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 2000,\"acc\": 8000,\"dec\": 8000,\"position\": 0},{\"id\": 1,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 16000,\"acc\": 16000,\"dec\": 16000,\"position\": 6350},{\"id\": 1,\"pre_wait\": 3000,\"ele\": \"xr_r\",\"speed\": 4000,\"acc\": 4000,\"dec\": 4000,\"position\": 4200}]},\"gotoTarY\": {\"electl\": [{\"id\": 1,\"pre_wait\": 1000,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4140},{\"id\": 2,\"pre_wait\": 1000,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4750}]},\"gotoTarY2\": {\"electl\": [{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4140},{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 1000,\"acc\": 800,\"dec\": 800,\"position\": 4750}]},\"downTar\": {\"electl\": {\"id\": 1,\"ele\": \"xr_z\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4100}},\"jawopen\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_jaw\",\"speed\": 500,\"acc\": 500,\"dec\": 500,\"position\": 0},{\"id\": 2,\"ele\": \"xr_y\",\"speed\": 3000,\"acc\": 3000,\"dec\": 3000,\"position\": 4140}]},\"goreset1\": {\"electl\": [{\"id\": 1,\"ele\": \"xr_z\",\"speed\": 1000,\"acc\": 1000,\"dec\": 1000,\"position\": 0},{\"id\": 2,\"pre_wait\": 2000,\"ele\": \"xr_r\",\"speed\": 4000,\"acc\": 4000,\"dec\": 4000,\"position\": 120},{\"id\": 2,\"pre_wait\": 2000,\"ele\": \"xr_y\",\"speed\": 16000,\"acc\": 16000,\"dec\": 16000,\"position\": 6350}]},\"goreset2\": {\"electl\": [{\"id\": 2,\"ele\": \"xr_r\",\"speed\": 250,\"acc\": 200,\"dec\": 200,\"position\": 0},{\"id\": 2,\"pre_wait\": 2000,\"ele\": \"xr_y\",\"speed\": 1000,\"acc\": 4000,\"dec\": 1000,\"position\": 0}]}}}"
            })
                .then(function(response){
                    console.log("设置热封闲置位夹取到DJ位参数成功")
                })
                .catch(function(err){
                    alert(err);
                })
        },

 //发送XR夹取运动指令
        XRworkorder:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/module/XR_AToB", {})
                .then(function(response){
                    if(response.data.errorNum == 100){
                        console.log("发送XR夹取运动指令成功");
                    }else{
                        alert(response.data.Des);
                    }
                })
                .catch(function(err){
                    alert(err);
                })
        },

//发送热封运动指令
        Heatworkorder:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/module/Heat_Seal", {})
                .then(function(response){
                    if(response.data.errorNum == 100){
                        console.log("发送热封运动指令成功");
                    }else{
                        alert(response.data.Des);
                    }
                })
                .catch(function(err){
                    alert(err);
                })
        },



//循环流程1


        async XRwithHeatold(){
            await this.XRktoDJposi();             //空板夹取到DJ位参数
            await this. XRworkorder();             //运动
            await this.XRDJtokposi();             //DJ位夹取到空板位参数
            await this.XRworkorder();             //运动
            await this.XRktoHeatposi();               //空板夹取到热封闲置位参数
            await this.XRworkorder();             //运动
            await this.XRHeatposi();            //热封参数
            await this.Heatworkorder();                //运动
            await this.XRHeattoDJposi();            //热封闲置位夹取到DJ位参数
            await this.XRworkorder();            //运动
            await this.XRDJtokposi();             //DJ位夹取到空板位参数
            await this.XRworkorder();             //运动
            await this.XRwithHeatold();
        },



//循环流程2
        async XRwithHeatold1(){
            await this.XRHeattoDJposi();            //热封闲置位夹取到DJ位参数
            await this.XRworkorder();            //运动
            await this.XRDJtokposi();             //DJ位夹取到空板位参数
            await this.XRworkorder();             //运动
        },

        // qpcrStart: function () {
        //     let _s = this;
        //     axios.put("http://10.6.3.74:8082/v1/rdml/run_rdml", _s.qpcr.Preceding + _s.qpcr.CyclerTime + _s.qpcr.Post,{headers: {'Content-Type': 'application/json'}})
        //         .then(function (res) {
        //             if (res.data.Status == "success") {
        //                 console.log("成了，次数" + _s.qpcr.CyclerTime);
        //             } else {
        //                 console.log("发错东西了")
        //             }
        //         })
        //         .catch(function (err) {
        //             alert(err);
        //         })
        //
        // }




//发送DP8三轴回100原点参数
        XYZhomeposi:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 5,
                "filesize": 2824,
                "chunk": "{  \"dp8\": {  \"z_ret\": {\"electl\": {\"id\": 3,\"ele\": \"dp8_z\",\"speed\": 10000,\"acc\": 10000,\"dec\": 10000,\"position\": 0}},  \"loc_horizen\": {      \"electl\": [        {          \"id\": 1,          \"pre_wait\": 2000,          \"ele\": \"dp8_x\",          \"speed\": 8000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 100        },        {          \"id\": 2,          \"pre_wait\": 2000,          \"ele\": \"dp8_y\",          \"speed\": 8000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 100        },        {          \"id\": 3,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 0        }      ]    },    \"loc_vertical\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 0        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 8000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 0        }      ]    },    \"probe\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 4000,        \"acc\": 4000,        \"dec\": 4000,        \"position\": 80000      }    },    \"suck_act\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 2000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 2000        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 4000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 2000        }      ]    },    \"suck_end\": {      \"electl\": {        \"id\": 1,    \"pre_wait\": 3000,    \"ele\": \"dp8_z\",        \"speed\": 2000,        \"acc\": 4000,        \"dec\": 4000,        \"position\": 0      }    },    \"droptip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 8000,        \"acc\": 4000,        \"dec\": 4000,        \"position\": 83000      }    },    \"droptip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 8000,        \"acc\": 4000,        \"dec\": 4000,        \"position\": 40000      }    },    \"spit_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 8000,        \"acc\": 4000,        \"dec\": 4000,        \"position\": 40000      }    },    \"spit_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 8000,        \"acc\": 4000,        \"dec\": 4000,        \"position\": 20000      }    },    \"ontip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 4000,        \"dec\": 4000,        \"position\": 167580      }    },    \"ontip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 4000,        \"dec\": 4000,        \"position\": 40000      }    }  }}"
            })
                .then(function(response){
                    console.log("设置三轴原点参数成功")
                })
                .catch(function(err){
                    alert(err);
                })
        },


//发送DP8三轴回100原点及suke80000参数
//         XYZhomeSuke80000posi:function () {
//             let _s = this;
//             return   axios.post(_s.url + "/v1/param/to_board", {
//                 "fileId": 5,
//                 "filesize": 2819,
//                 "chunk": "{  \"dp8\": {  \"z_ret\": {\"electl\": {\"id\": 3,\"ele\": \"dp8_z\",\"speed\": 10000,\"acc\": 10000,\"dec\": 10000,\"position\": 0}},  \"loc_horizen\": {      \"electl\": [        {          \"id\": 1,          \"pre_wait\": 2000,          \"ele\": \"dp8_x\",          \"speed\": 8000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 100        },        {          \"id\": 2,          \"pre_wait\": 2000,          \"ele\": \"dp8_y\",          \"speed\": 8000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 100        },        {          \"id\": 3,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 0        }      ]    },    \"loc_vertical\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 0        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 8000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 80000        }      ]    },    \"probe\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 80000      }    },    \"suck_act\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 10000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 2000        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 4000,          \"acc\": 8000,          \"dec\": 8000,          \"position\": 2000        }      ]    },    \"suck_end\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    },    \"droptip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 8000,        \"acc\": 8000,        \"dec\": 8000,        \"position\": 83000      }    },    \"droptip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 4000,        \"acc\": 8000,        \"dec\": 8000,        \"position\": 40000      }    },    \"spit_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 4000,        \"acc\": 8000,        \"dec\": 8000,        \"position\": 86000      }    },    \"spit_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 4000,        \"acc\": 8000,        \"dec\": 8000,        \"position\": 80000      }    },    \"ontip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 4000,        \"dec\": 4000,        \"position\": 167580      }    },    \"ontip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    }  }}"
//             })
//                 .then(function(response){
//                     console.log("设置DP8三轴回100原点及suke80000参数成功")
//                 })
//                 .catch(function(err){
//                     alert(err);
//                 })
//         },


//发送三轴运动指令
        XYZmovetopw:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/module/DP8_MOVE", {})
                .then(function(response){
                    if(response.data.errorNum == 100){
                        console.log("发送三轴运动指令成功");
                    }else{
                        alert(response.data.Des);
                    }
                })
                .catch(function(err){
                    alert(err);
                })
        },

//发送取Tip头1位置参数
        XYZtip1posi:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 5,
                "filesize": 2840,
                "chunk": "{  \"dp8\": { \"z_ret\": {\"electl\": {\"id\": 3,\"ele\": \"dp8_z\",\"speed\": 10000,\"acc\": 10000,\"dec\": 10000,\"position\": 0}},   \"loc_horizen\": {      \"electl\": [        {          \"id\": 1,          \"pre_wait\": 2000,          \"ele\": \"dp8_x\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 5561        },        {          \"id\": 2,          \"pre_wait\": 2000,          \"ele\": \"dp8_y\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 21842        },        {          \"id\": 3,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 0        }      ]    },    \"loc_vertical\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 20000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 126000        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 8000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 80000        }      ]    },    \"probe\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 4000,        \"acc\": 4000,        \"dec\": 4000,        \"position\": 80000      }    },    \"suck_act\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 2000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 2000        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 4000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 2000        }      ]    },    \"suck_end\": {      \"electl\": {        \"id\": 1,    \"pre_wait\": 3000,    \"ele\": \"dp8_z\",        \"speed\": 2000,        \"acc\": 4000,        \"dec\": 4000,        \"position\": 0      }    },    \"droptip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 104580      }    },    \"droptip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    },    \"spit_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 8000,        \"acc\": 4000,        \"dec\": 4000,        \"position\": 40000      }    },    \"spit_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 8000,        \"acc\": 4000,        \"dec\": 4000,        \"position\": 20000      }    },    \"ontip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 166320      }    },    \"ontip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    }  }}"
            })
                .then(function(response){
                    console.log("设置Tip头1位置参数成功")
                })
                .catch(function(err){
                    alert(err);
                })
        },

//发送取Tip头2位置参数
        XYZtip2posi:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 5,
                "filesize": 2840,
                "chunk": "{  \"dp8\": { \"z_ret\": {\"electl\": {\"id\": 3,\"ele\": \"dp8_z\",\"speed\": 10000,\"acc\": 10000,\"dec\": 10000,\"position\": 0}},   \"loc_horizen\": {      \"electl\": [        {          \"id\": 1,          \"pre_wait\": 2000,          \"ele\": \"dp8_x\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 6164        },        {          \"id\": 2,          \"pre_wait\": 2000,          \"ele\": \"dp8_y\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 21842        },        {          \"id\": 3,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 0        }      ]    },    \"loc_vertical\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 20000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 126000        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 8000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 80000        }      ]    },    \"probe\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 4000,        \"acc\": 4000,        \"dec\": 4000,        \"position\": 80000      }    },    \"suck_act\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 2000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 2000        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 4000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 2000        }      ]    },    \"suck_end\": {      \"electl\": {        \"id\": 1,  \"pre_wait\": 3000,      \"ele\": \"dp8_z\",        \"speed\": 2000,        \"acc\": 4000,        \"dec\": 4000,        \"position\": 0      }    },    \"droptip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 104580      }    },    \"droptip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    },    \"spit_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 8000,        \"acc\": 4000,        \"dec\": 4000,        \"position\": 40000      }    },    \"spit_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 8000,        \"acc\": 4000,        \"dec\": 4000,        \"position\": 20000      }    },    \"ontip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 166320      }    },    \"ontip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    }  }}"
            })
                .then(function(response){
                    console.log("设置Tip头2位置参数成功")
                })
                .catch(function(err){
                    alert(err);
                })
        },


//发送取tip头1指令
        Gtip1order:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/module/DP8_TipOn", {})
                .then(function(response){
                    if(response.data.errorNum == 100){
                        console.log("发送取tip头指令成功");
                    }else{
                        alert(response.data.Des);
                    }
                })
                .catch(function(err){
                    alert(err);
                })
        },

//发送裂解液位置参数
        XYZliejieyposi:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 5,
                "filesize": 2854,
                "chunk": "{  \"dp8\": { \"z_ret\": {\"electl\": {\"id\": 3,\"ele\": \"dp8_z\",\"speed\": 10000,\"acc\": 10000,\"dec\": 10000,\"position\": 0}},   \"loc_horizen\": {      \"electl\": [        {          \"id\": 1,          \"pre_wait\": 2000,          \"ele\": \"dp8_x\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 5628        },        {          \"id\": 2,          \"pre_wait\": 2000,          \"ele\": \"dp8_y\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 28140        },        {          \"id\": 3,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 0        }      ]    },    \"loc_vertical\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 20000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 0        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 8000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 80000        }      ]    },    \"probe\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 10000,        \"acc\": 80000,        \"dec\": 80000,        \"position\": 79380      }    },    \"suck_act\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 11340        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 20000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 30240        }      ]    },    \"suck_end\": {      \"electl\": {        \"id\": 1,    \"pre_wait\": 10000,    \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 80000,        \"dec\": 80000,        \"position\": 0      }    },    \"droptip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 105840      }    },    \"droptip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    },    \"spit_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 86000      }    },    \"spit_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 20000      }    },    \"ontip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 167580      }    },    \"ontip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    }  }}"
            })
                .then(function(response){
                    console.log("设置裂解位参数成功")
                })
                .catch(function(err){
                    alert(err);
                })
        },

//发送吸裂解液指令
        Sukeliejieyorder:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/module/DP8_SUCK", {})
                .then(function(response){
                    if(response.data.errorNum == 100){
                        console.log("发送吸裂解液指令成功");
                    }else{
                        alert(response.data.Des);
                    }
                })
                .catch(function(err){
                    alert(err);
                })
        },

//发送DP8内部震荡加液位位置参数
        XYZneishakeposi1:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 5,
                "filesize": 2858,
                "chunk": "{  \"dp8\": { \"z_ret\": {\"electl\": {\"id\": 3,\"ele\": \"dp8_z\",\"speed\": 10000,\"acc\": 10000,\"dec\": 10000,\"position\": 0}},   \"loc_horizen\": {      \"electl\": [        {          \"id\": 1,          \"pre_wait\": 2000,          \"ele\": \"dp8_x\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 536        },        {          \"id\": 2,          \"pre_wait\": 2000,          \"ele\": \"dp8_y\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 21641        },        {          \"id\": 3,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 0        }      ]    },    \"loc_vertical\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 20000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 37800        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 8000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 49760        }      ]    },    \"probe\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 10000,        \"acc\": 80000,        \"dec\": 80000,        \"position\": 148680      }    },    \"suck_act\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 11340        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 20000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 30240        }      ]    },    \"suck_end\": {      \"electl\": {        \"id\": 1,   \"pre_wait\": 10000,     \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 80000,        \"dec\": 80000,        \"position\": 0      }    },    \"droptip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 105840      }    },    \"droptip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    },    \"spit_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 86000      }    },    \"spit_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 80000      }    },    \"ontip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 167580      }    },    \"ontip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    }  }}"
            })
                .then(function(response){
                    console.log("设置内部震荡位参数成功")
                })
                .catch(function(err){
                    alert(err);
                })
        },

//发送DP8内部震荡加液位2位置参数
        XYZneishakeposi2:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 5,
                "filesize": 2858,
                "chunk": "{  \"dp8\": { \"z_ret\": {\"electl\": {\"id\": 3,\"ele\": \"dp8_z\",\"speed\": 10000,\"acc\": 10000,\"dec\": 10000,\"position\": 0}},   \"loc_horizen\": {      \"electl\": [        {          \"id\": 1,          \"pre_wait\": 2000,          \"ele\": \"dp8_x\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 536        },        {          \"id\": 2,          \"pre_wait\": 2000,          \"ele\": \"dp8_y\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 21641        },        {          \"id\": 3,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 0        }      ]    },    \"loc_vertical\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 20000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 37800        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 8000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 23300        }      ]    },    \"probe\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 10000,        \"acc\": 80000,        \"dec\": 80000,        \"position\": 148680      }    },    \"suck_act\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 11340        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 20000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 56700        }      ]    },    \"suck_end\": {      \"electl\": {        \"id\": 1,   \"pre_wait\": 10000,     \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 80000,        \"dec\": 80000,        \"position\": 0      }    },    \"droptip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 105840      }    },    \"droptip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    },    \"spit_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 86000      }    },    \"spit_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 80000      }    },    \"ontip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 167580      }    },    \"ontip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    }  }}"
            })
                .then(function(response){
                    console.log("设置DP8内部震荡加液位2位置参数成功")
                })
                .catch(function(err){
                    alert(err);
                })
        },

//发送DP8内部震荡加液位3位置参数
        XYZneishakeposi3:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 5,
                "filesize": 2857,
                "chunk": "{  \"dp8\": { \"z_ret\": {\"electl\": {\"id\": 3,\"ele\": \"dp8_z\",\"speed\": 10000,\"acc\": 10000,\"dec\": 10000,\"position\": 0}},   \"loc_horizen\": {      \"electl\": [        {          \"id\": 1,          \"pre_wait\": 2000,          \"ele\": \"dp8_x\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 536        },        {          \"id\": 2,          \"pre_wait\": 2000,          \"ele\": \"dp8_y\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 21641        },        {          \"id\": 3,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 0        }      ]    },    \"loc_vertical\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 20000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 78338        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 8000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 72440        }      ]    },    \"probe\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 10000,        \"acc\": 80000,        \"dec\": 80000,        \"position\": 148680      }    },    \"suck_act\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 11340        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 20000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 7560        }      ]    },    \"suck_end\": {      \"electl\": {        \"id\": 1,   \"pre_wait\": 10000,     \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 80000,        \"dec\": 80000,        \"position\": 0      }    },    \"droptip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 105840      }    },    \"droptip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    },    \"spit_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 86000      }    },    \"spit_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 80000      }    },    \"ontip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 167580      }    },    \"ontip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    }  }}"
            })
                .then(function(response){
                    console.log("设置DP8内部震荡加液位3位置参数成功")
                })
                .catch(function(err){
                    alert(err);
                })
        },

//发送喷裂解液指令
        Spitliejieyorder:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/module/DP8_SPIT", {})
                .then(function(response){
                    if(response.data.errorNum == 100){
                        console.log("发送喷裂解液指令成功");
                    }else{
                        alert(response.data.Des);
                    }
                })
                .catch(function(err){
                    alert(err);
                })
        },


//发送Z轴安全位指令
        SafeZmoveto0:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/dp8/z", {
                "position": 0,
                "speed": 10000,
                "acc": 10000,
                "dec": 10000
            })
                .then(function(response){
                    if(response.data.errorNum == 100){
                        console.log("发送Z轴安全位指令成功");
                    }else{
                        alert(response.data.Des);
                    }
                })
                .catch(function(err){
                    alert(err);
                })
        },


//发送废液位1位置参数
        XYZwasteposi1:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 5,
                "filesize": 2853,
                "chunk": "{  \"dp8\": {  \"z_ret\": {\"electl\": {\"id\": 3,\"ele\": \"dp8_z\",\"speed\": 10000,\"acc\": 10000,\"dec\": 10000,\"position\": 0}},  \"loc_horizen\": {      \"electl\": [        {          \"id\": 1,          \"pre_wait\": 2000,          \"ele\": \"dp8_x\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 9849        },        {          \"id\": 2,          \"pre_wait\": 2000,          \"ele\": \"dp8_y\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 28140        },        {          \"id\": 3,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 0        }      ]    },    \"loc_vertical\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 20000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 0        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 8000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 49760        }      ]    },    \"probe\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 80000,        \"dec\": 80000,        \"position\": 79380      }    },    \"suck_act\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 12000        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 20000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 12000        }      ]    },    \"suck_end\": {      \"electl\": {        \"id\": 1,    \"pre_wait\": 10000,    \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    },    \"droptip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 105840      }    },    \"droptip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    },    \"spit_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 86000      }    },    \"spit_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 80000      }    },    \"ontip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 167580      }    },    \"ontip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    }  }}"
            })
                .then(function(response){
                    console.log("设置废液位1位置参数成功")
                })
                .catch(function(err){
                    alert(err);
                })
        },

//发送废液位11位置参数
        XYZwasteposi11:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 5,
                "filesize": 2853,
                "chunk": "{  \"dp8\": {  \"z_ret\": {\"electl\": {\"id\": 3,\"ele\": \"dp8_z\",\"speed\": 10000,\"acc\": 10000,\"dec\": 10000,\"position\": 0}},  \"loc_horizen\": {      \"electl\": [        {          \"id\": 1,          \"pre_wait\": 2000,          \"ele\": \"dp8_x\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 9849        },        {          \"id\": 2,          \"pre_wait\": 2000,          \"ele\": \"dp8_y\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 28140        },        {          \"id\": 3,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 0        }      ]    },    \"loc_vertical\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 20000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 0        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 8000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 23300        }      ]    },    \"probe\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 80000,        \"dec\": 80000,        \"position\": 79380      }    },    \"suck_act\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 12000        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 20000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 12000        }      ]    },    \"suck_end\": {      \"electl\": {        \"id\": 1,    \"pre_wait\": 10000,    \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    },    \"droptip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 105840      }    },    \"droptip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    },    \"spit_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 86000      }    },    \"spit_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 80000      }    },    \"ontip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 167580      }    },    \"ontip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    }  }}"
            })
                .then(function(response){
                    console.log("设置废液位11位置参数成功")
                })
                .catch(function(err){
                    alert(err);
                })
        },

//发送废液位12位置参数
        XYZwasteposi12:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 5,
                "filesize": 2854,
                "chunk": "{  \"dp8\": {  \"z_ret\": {\"electl\": {\"id\": 3,\"ele\": \"dp8_z\",\"speed\": 10000,\"acc\": 10000,\"dec\": 10000,\"position\": 0}},  \"loc_horizen\": {      \"electl\": [        {          \"id\": 1,          \"pre_wait\": 2000,          \"ele\": \"dp8_x\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 10452        },        {          \"id\": 2,          \"pre_wait\": 2000,          \"ele\": \"dp8_y\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 28140        },        {          \"id\": 3,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 0        }      ]    },    \"loc_vertical\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 20000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 0        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 8000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 23300        }      ]    },    \"probe\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 80000,        \"dec\": 80000,        \"position\": 79380      }    },    \"suck_act\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 12000        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 20000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 12000        }      ]    },    \"suck_end\": {      \"electl\": {        \"id\": 1,    \"pre_wait\": 10000,    \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    },    \"droptip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 105840      }    },    \"droptip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    },    \"spit_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 86000      }    },    \"spit_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 80000      }    },    \"ontip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 167580      }    },    \"ontip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    }  }}"
            })
                .then(function(response){
                    console.log("设置废液位11位置参数成功")
                })
                .catch(function(err){
                    alert(err);
                })
        },

//发送DNA位置参数
        XYZDNAposi:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 5,
                "filesize": 2852,
                "chunk": "{  \"dp8\": {  \"z_ret\": {\"electl\": {\"id\": 3,\"ele\": \"dp8_z\",\"speed\": 10000,\"acc\": 10000,\"dec\": 10000,\"position\": 0}},  \"loc_horizen\": {      \"electl\": [        {          \"id\": 1,          \"pre_wait\": 2000,          \"ele\": \"dp8_x\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 12261        },        {          \"id\": 2,          \"pre_wait\": 2000,          \"ele\": \"dp8_y\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 28140        },        {          \"id\": 3,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 0        }      ]    },    \"loc_vertical\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 20000,          \"acc\": 78338,          \"dec\": 80000,          \"position\": 0        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 8000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 72440        }      ]    },    \"probe\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 79380      }    },    \"suck_act\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 8000,          \"dec\": 8000,          \"position\": 12000        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 20000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 12000        }      ]    },    \"suck_end\": {      \"electl\": {        \"id\": 1,     \"pre_wait\": 10000,   \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    },    \"droptip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 105840      }    },    \"droptip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    },    \"spit_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 86000      }    },    \"spit_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 80000      }    },    \"ontip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 167580      }    },    \"ontip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    }  }}"
            })
                .then(function(response){
                    console.log("设置DNA位置参数成功")
                })
                .catch(function(err){
                    alert(err);
                })
        },

//发送清洗液1位置参数
        XYZwashposi1:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 5,
                "filesize": 2853,
                "chunk": "{  \"dp8\": {  \"z_ret\": {\"electl\": {\"id\": 3,\"ele\": \"dp8_z\",\"speed\": 10000,\"acc\": 10000,\"dec\": 10000,\"position\": 0}},  \"loc_horizen\": {      \"electl\": [        {          \"id\": 1,          \"pre_wait\": 2000,          \"ele\": \"dp8_x\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 6231        },        {          \"id\": 2,          \"pre_wait\": 2000,          \"ele\": \"dp8_y\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 28140        },        {          \"id\": 3,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 0        }      ]    },    \"loc_vertical\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 20000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 0        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 8000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 80000        }      ]    },    \"probe\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 80000,        \"dec\": 80000,        \"position\": 79380      }    },    \"suck_act\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 11340        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 20000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 56700        }      ]    },    \"suck_end\": {      \"electl\": {        \"id\": 1,    \"pre_wait\": 10000,    \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 80000,        \"dec\": 80000,        \"position\": 0      }    },    \"droptip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 105840      }    },    \"droptip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    },    \"spit_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 86000      }    },    \"spit_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 20000      }    },    \"ontip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 167580      }    },    \"ontip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    }  }}"
            })
                .then(function(response){
                    console.log("设置清洗液1位置参数成功")
                })
                .catch(function(err){
                    alert(err);
                })
        },

//发送清洗液2位置参数
        XYZwashposi2:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 5,
                "filesize": 2853,
                "chunk": "{  \"dp8\": {  \"z_ret\": {\"electl\": {\"id\": 3,\"ele\": \"dp8_z\",\"speed\": 10000,\"acc\": 10000,\"dec\": 10000,\"position\": 0}},  \"loc_horizen\": {      \"electl\": [        {          \"id\": 1,          \"pre_wait\": 2000,          \"ele\": \"dp8_x\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 6834        },        {          \"id\": 2,          \"pre_wait\": 2000,          \"ele\": \"dp8_y\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 28140        },        {          \"id\": 3,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 0        }      ]    },    \"loc_vertical\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 20000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 0        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 8000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 80000        }      ]    },    \"probe\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 80000,        \"dec\": 80000,        \"position\": 79380      }    },    \"suck_act\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 11340        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 20000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 56700        }      ]    },    \"suck_end\": {      \"electl\": {        \"id\": 1,    \"pre_wait\": 10000,    \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 80000,        \"dec\": 80000,        \"position\": 0      }    },    \"droptip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 105840      }    },    \"droptip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    },    \"spit_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 86000      }    },    \"spit_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 20000      }    },    \"ontip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 167580      }    },    \"ontip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    }  }}"
            })
                .then(function(response){
                    console.log("设置清洗液2位置参数成功")
                })
                .catch(function(err){
                    alert(err);
                })
        },


//发送洗脱液位置参数
        XYZwashcleanposi:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 5,
                "filesize": 2852,
                "chunk": "{  \"dp8\": {  \"z_ret\": {\"electl\": {\"id\": 3,\"ele\": \"dp8_z\",\"speed\": 10000,\"acc\": 10000,\"dec\": 10000,\"position\": 0}},  \"loc_horizen\": {      \"electl\": [        {          \"id\": 1,          \"pre_wait\": 2000,          \"ele\": \"dp8_x\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 7437        },        {          \"id\": 2,          \"pre_wait\": 2000,          \"ele\": \"dp8_y\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 28140        },        {          \"id\": 3,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 0        }      ]    },    \"loc_vertical\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 20000,          \"acc\": 80000,          \"dec\": 80000,          \"position\": 0        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 8000,          \"acc\": 4000,          \"dec\": 4000,          \"position\": 80000        }      ]    },    \"probe\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 80000,        \"dec\": 80000,        \"position\": 79380      }    },    \"suck_act\": {      \"electl\": [        {          \"id\": 1,          \"ele\": \"dp8_z\",          \"speed\": 8000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 12000        },        {          \"id\": 2,          \"ele\": \"dp8_suck\",          \"speed\": 20000,          \"acc\": 10000,          \"dec\": 10000,          \"position\": 7560        }      ]    },    \"suck_end\": {      \"electl\": {        \"id\": 1,    \"pre_wait\": 10000,    \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    },    \"droptip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 105840      }    },    \"droptip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 10000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    },    \"spit_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 86000      }    },    \"spit_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_suck\",        \"speed\": 20000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 80000      }    },    \"ontip_down\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 167580      }    },    \"ontip_up\": {      \"electl\": {        \"id\": 1,        \"ele\": \"dp8_z\",        \"speed\": 8000,        \"acc\": 10000,        \"dec\": 10000,        \"position\": 0      }    }  }}"
            })
                .then(function(response){
                    console.log("设置洗脱液位置参数成功")
                })
                .catch(function(err){
                    alert(err);
                })
        },

//发送退Tip头指令
        Ptip1order:function () {
            let _s = this;
            return   axios.post(_s.url + "/v1/module/DP8_TipDown", {})
                .then(function(response){
                    if(response.data.errorNum == 100){
                        console.log("发送退Tip头指令成功");
                    }else{
                        alert(response.data.Des);
                    }
                })
                .catch(function(err){
                    alert(err);
                })
        },




        async DP8old1(){
            await this.XYZhomeposi();             //原点100位
            await this.XYZmovetopw();             //运动
            await this.XYZtip1posi();             //Tip1位
            await this.XYZmovetopw();             //运动
            await this.Gtip1order();               //取Tip1

            await this.XYZliejieyposi();            //裂解液位
            await this.XYZmovetopw();                //运动
            await this.Sukeliejieyorder();            //吸裂解液
            await this.XYZneishakeposi1();            //内部震荡位1（裂解液量吸废液参数）
            await this.XYZmovetopw();               //运动
            await this.Spitliejieyorder();            //喷液
            await this.SafeZmoveto0();            //Z轴抬起到0

        },
        async DP8old2(){
            // //缺内部加热+震荡+吸磁
            await this.Sukeliejieyorder();            //吸裂解液废液
            await this.XYZwasteposi1();                //喷废液1位置
            await this.XYZmovetopw();               //运动
            await this.Spitliejieyorder();            //喷液
            //
            await this.XYZwashposi1();               //清洗液1位
            await this.XYZmovetopw();               //运动
            await this.Sukeliejieyorder();            //吸清洗液1
            await this.XYZneishakeposi2();            //内部震荡位2（清洗液1量吸废液参数）
            await this.XYZmovetopw();               //运动
            await this.Spitliejieyorder();            //喷液
            await this.SafeZmoveto0();            //Z轴抬起到0

        },
        async DP8old3(){



            // //缺内部加热+震荡+吸磁
            await this.Sukeliejieyorder();            //吸清洗液1废液
            await this.XYZwasteposi11();                //喷废液11位置
            await this.XYZmovetopw();               //运动
            await this.Spitliejieyorder();            //喷液
            //
            //
            await this.XYZwashposi2();               //清洗液位2
            await this.XYZmovetopw();               //运动
            await this.Sukeliejieyorder();            //吸清洗液2
            await this.XYZneishakeposi2();            //内部震荡位2（清洗液2量吸废液参数）
            await this.XYZmovetopw();               //运动
            await this.Spitliejieyorder();            //喷液
            await this.SafeZmoveto0();            //Z轴抬起到0

        },
        async DP8old4(){

            //缺内部加热+震荡+吸磁
            await this.Sukeliejieyorder();            //吸清洗液2废液
            await this.XYZwasteposi12();                //喷废液12位置
            await this.XYZmovetopw();               //运动
            await this.Spitliejieyorder();            //喷液
            //
            await this.XYZtip1posi();             //Tip1位
            await this.XYZmovetopw();             //运动
            await this.Ptip1order();               //退Tip
            // // 取消吸磁
            // // 放置7min除醇
            //

        },
            //
            //
            //
            //
        async DP8old5(){
            await this.XYZtip2posi();               //取 Tip2位
            await this.XYZmovetopw();               //运动
            await this.Gtip1order();               //取Tip2
            await this.XYZwashcleanposi();               //洗脱液位
            await this.XYZmovetopw();               //运动
            await this.Sukeliejieyorder();            //吸洗脱液
            await this.XYZneishakeposi3();            //内部震荡位3（洗脱液量吸废液参数）
            await this.XYZmovetopw();               //运动
            await this.Spitliejieyorder();            //喷液
            await this.SafeZmoveto0();            //Z轴抬起到0
        },
        async DP8old6(){

            // //缺内部加热+震荡+吸磁
            await this.Sukeliejieyorder();            //吸洗脱液废液
            await this.XYZDNAposi();                //喷DNA位置
            await this.XYZmovetopw();               //运动
            await this.Spitliejieyorder();            //喷液
            await this.XYZtip2posi();               //Tip2位
            await this.XYZmovetopw();               //运动
            await this.Ptip1order();               //退Tip


        },











        fResJudge: function (res) {
            switch (res.data.errorNum) {
                case 100:
                    console.log("动作成功");
                    break;
                // case 1:
                //     switch (res.data.errorNum) {
                //         case 1:
                //             alert("超出行程");
                //             break;
                //         case 2:
                //             alert("超时");
                //             break;
                //         case 3:
                //             alert("控制板信息错误");
                //             break;
                //     }
                case 2:
                    alert("动作超时");
                    break;
                default:
                    alert("动作出错");
            }
        },

        fchangeState: function (tar) {
            tag = document.getElementById(tar);
            tag.classList.remove("Uninited");
            tag.classList.add("Inited");
        },

        fDP8XInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/dp8/x",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DP8.X")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        fDP8YInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/dp8/y",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DP8.Y")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        fDP8ZInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/dp8/z",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DP8.Z")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        fDP8suckInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/dp8/suck_liquid",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DP8.suck")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },


        fSealZInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/thermal_seal/z",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("Seal.Z")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },

        fOpenLidZInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/openLid/z",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("openLid.Z")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },


        fOpenLidYInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/openLid/y",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("openLid.Y")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        fOpenLidjawInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/openLid/jaw",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("openLid.jaw")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //DP1夹爪初始化
        fDP1JawInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/dp1/drop_tube_draw",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DP1.Jaw")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //DP1夹爪Z初始化
        fDP1JawZInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/dp1/jaw_z",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DP1.JawZ")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //DP1X初始化
        fDP1XInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/dp1/x",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DP1.X")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //DP1Y初始化
        fDP1YInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/dp1/y",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DP1.Y")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },


        fXRZInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/xrModule/z",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("XR.Z")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        fXRYInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/xrModule/y",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("XR.Z")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        fXRjawInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/xrModule/jaw",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("XR.Z")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        fXRrotateInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/xrModule/rotate",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("XR.rotate")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },


        fOpenLidY: function () {
            let _s = this;
            axios.post(_s.url + "/v1/openLid/y",
                {
                    "position": _s.openLid.Y.position,
                    "speed": _s.openLid.Y.speed,
                    "acc": _s.openLid.Y.acc,
                    "dec": _s.openLid.Y.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })


        },
        fOpenLidjaw: function () {
            let _s = this;
            axios.post(_s.url + "/v1/openLid/jaw",
                {
                    "position": _s.openLid.jaw.position,
                    "speed": _s.openLid.jaw.speed,
                    "acc": _s.openLid.jaw.acc,
                    "dec": _s.openLid.jaw.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })


        },
        fOpenLidZ: function () {
            let _s = this;
            axios.post(_s.url + "/v1/openLid/z",
                {
                    "position": _s.openLid.Z.position,
                    "speed": _s.openLid.Z.speed,
                    "acc": _s.openLid.Z.acc,
                    "dec": _s.openLid.Z.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        fScanCode: function () {
            let _s = this;
            axios.post(_s.url + "/v1/scan_mode/rotate",
                {
                    "position": _s.ScanCode.position,
                    "speed": _s.ScanCode.speed,
                    "acc": _s.ScanCode.acc,
                    "dec": _s.ScanCode.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        fXRZ: function () {
            let _s = this;
            axios.post(_s.url + "/v1/xrModule/z",
                {
                    "position": _s.XR.Z.position,
                    "speed": _s.XR.Z.speed,
                    "acc": _s.XR.Z.acc,
                    "dec": _s.XR.Z.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        fXRY: function () {
            let _s = this;
            axios.post(_s.url + "/v1/xrModule/y",
                {
                    "position": _s.XR.Y.position,
                    "speed": _s.XR.Y.speed,
                    "acc": _s.XR.Y.acc,
                    "dec": _s.XR.Y.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },

        fXRjaw: function () {
            let _s = this;
            axios.post(_s.url + "/v1/xrModule/jaw",
                {
                    "position": _s.XR.jaw.position,
                    "speed": _s.XR.jaw.speed,
                    "acc": _s.XR.jaw.acc,
                    "dec": _s.XR.jaw.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },

        fXRrotate: function () {
            let _s = this;
            axios.post(_s.url + "/v1/xrModule/rotate",
                {
                    "position": _s.XR.rotate.position,
                    "speed": _s.XR.rotate.speed,
                    "acc": _s.XR.rotate.acc,
                    "dec": _s.XR.rotate.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },


        fDP8X: function () {
            let _s = this;
            axios.post(_s.url + "/v1/dp8/x",
                {
                    "position": _s.DP8.X.position,
                    "speed": _s.DP8.X.speed,
                    "acc": _s.DP8.X.acc,
                    "dec": _s.DP8.X.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);


                })
                .catch(function (err) {
                    alert(err);
                })

        },
        fDP8Y: function () {
            let _s = this;
            axios.post(_s.url + "/v1/dp8/y",
                {
                    "position": _s.DP8.Y.position,
                    "speed": _s.DP8.Y.speed,
                    "acc": _s.DP8.Y.acc,
                    "dec": _s.DP8.Y.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        fDP8Z: function () {
            let _s = this;
            axios.post(_s.url + "/v1/dp8/z",
                {
                    "position": _s.DP8.Z.position,
                    "speed": _s.DP8.Z.speed,
                    "acc": _s.DP8.Z.acc,
                    "dec": _s.DP8.Z.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },


        fDP8suck: function () {
            let _s = this;
            axios.post(_s.url + "/v1/dp8/suck_liquid",
                {
                    "position": _s.DP8.suck.position,
                    "speed": _s.DP8.suck.speed,
                    "acc": _s.DP8.suck.acc,
                    "dec": _s.DP8.suck.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },

        fSealZ: function () {
            let _s = this;
            axios.post(_s.url + "/v1/thermal_seal/z",
                {
                    "position": _s.Seal.Z.position,
                    "speed": _s.Seal.Z.speed,
                    "acc": _s.Seal.Z.acc,
                    "dec": _s.Seal.Z.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //DP1夹爪
        fDP1Jaw: function () {
            let _s = this;
            axios.post(_s.url + "/v1/dp1/drop_Jaw_draw",
                {
                    "position": _s.DP1.Jaw.position,
                    "speed": _s.DP1.Jaw.speed,
                    "acc": _s.DP1.Jaw.acc,
                    "dec": _s.DP1.Jaw.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //DP1夹爪Z
        fDP1JawZ: function () {
            let _s = this;
            axios.post(_s.url + "v1/dp1/jaw_z",
                {
                    "position": _s.DP1.JawZ.position,
                    "speed": _s.DP1.JawZ.speed,
                    "acc": _s.DP1.JawZ.acc,
                    "dec": _s.DP1.JawZ.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //DP1X轴
        fDP1X: function () {
            let _s = this;
            axios.post(_s.url + "v1/dp1/x",
                {
                    "position": _s.DP1.X.position,
                    "speed": _s.DP1.X.speed,
                    "acc": _s.DP1.X.acc,
                    "dec": _s.DP1.X.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //DP1Y轴
        fDP1Y: function () {
            let _s = this;
            axios.post(_s.url + "v1/dp1/y",
                {
                    "position": _s.DP1.Y.position,
                    "speed": _s.DP1.Y.speed,
                    "acc": _s.DP1.Y.acc,
                    "dec": _s.DP1.Y.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },


        //开盖
        fOpenLid: function () {
            let _s = this;
            axios.post(_s.url + "/v1/module/OpenLid",
                {})
                .then(function (res) {
                    console.log(res);
                })
                .catch(function (err) {
                    alert(err)
                })


        },


        //关盖
        fCloseLid: function () {
            let _s = this;
            axios.post(_s.url + "/v1/module/CloseLid",
                {})
                .then(function (res) {
                    console.log(res);
                })
                .catch(function (err) {
                    alert(err)
                })


        },
        //AtoB
        fAtoB: function () {
            let _s = this;
            axios.post(_s.url + "/v1/module/XR_AToB",
                {})
                .then(function (res) {
                    console.log(res);
                })
                .catch(function (err) {
                    alert(err)
                })
        },


        //开盖、关盖循环
        EchoOpenLidStart: function () {
            let _s = this;
            _s.openLid.Echo.flag = true;
            _s.EchoOpenLid();
        },


        EchoOpenLid: function () {
            let _s = this;
            if (_s.openLid.Echo.flag == true) {
                axios.post(_s.url + "/v1/module/OpenLid",
                    {})
                    .then(function (res) {
                        console.log(res);
                        if (res.data.StateCode == null) {
                            _s.openLid.Echo.Step = "Close";
                            setTimeout(function () {
                                _s.EchoCloseLid();
                            }, 5000);
                        } else {
                            alert(res.data)
                        }

                    })
                    .catch(function (err) {
                        alert(err)
                    })
            }
        },
        EchoCloseLid: function () {
            let _s = this;
            if (_s.openLid.Echo.flag == true) {
                axios.post(_s.url + "/v1/module/CloseLid",
                    {})
                    .then(function (res) {
                        console.log(res);
                        if (res.data.StateCode == null) {
                            _s.openLid.Echo.Step = "Open";
                            setTimeout(function () {
                                _s.EchoOpenLid();
                            }, 5000);
                        } else {
                            alert(res.data)
                        }
                    })
                    .catch(function (err) {
                        alert(err)
                    })
            }
        },
        EchoOpenLidPause: function () {
            this.openLid.Echo.flag = false;
        },
        EchoOpenLidGoOn: function () {
            let _s = this;
            _s.openLid.Echo.flag = true;
            switch (_s.openLid.Echo.Step) {
                case "Close":
                    _s.EchoCloseLid();
                    break;
                case "Open":
                    _s.EchoOpenLid();
                    break;
                default:
                    console.log("GoOn函数错了");
            }

        },
        //DJ柱塞泵初始化
        fDJPumpInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/dj/pump",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DJ.Pump")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //DJX初始化
        fDJXInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/dj/x",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DJ.X")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //DJY初始化
        fDJYInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/dj/y",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DJ.Y")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },

        //DJZ初始化
        fDJZInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/dj/z",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DJ.Z")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },

        //物流X初始化
        fLogisticsXInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/logistics/x",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("Logistics.X")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //物流夹爪初始化
        fLogisticsJawInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/logistics/jaw",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("Logistics.Jaw")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //物流夹爪Y初始化
        fLogisticsJawYInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/logistics/jaw_y",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("Logistics.JawY")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //物流夹爪Z初始化
        fLogisticsJawZInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/logistics/jaw_z",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("Logistics.JawZ")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },

        //物流X
        fLogisticsX: function () {
            let _s = this;
            axios.post(_s.url + "v1/logistics/x",
                {
                    "position": _s.Logistics.X.position,
                    "speed": _s.Logistics.X.speed,
                    "acc": _s.Logistics.X.acc,
                    "dec": _s.Logistics.X.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },

        //物流夹爪
        fLogisticsJaw: function () {
            let _s = this;
            axios.post(_s.url + "v1/logistics/jaw",
                {
                    "position": _s.Logistics.Jaw.position,
                    "speed": _s.Logistics.Jaw.speed,
                    "acc": _s.Logistics.Jaw.acc,
                    "dec": _s.Logistics.Jaw.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //物流夹爪Y
        fLogisticsJawY: function () {
            let _s = this;
            axios.post(_s.url + "v1/logistics/jaw_y",
                {
                    "position": _s.Logistics.JawY.position,
                    "speed": _s.Logistics.JawY.speed,
                    "acc": _s.Logistics.JawY.acc,
                    "dec": _s.Logistics.JawY.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //物流夹爪Z
        fLogisticsJawZ: function () {
            let _s = this;
            axios.post(_s.url + "v1/logistics/jaw_z",
                {
                    "position": _s.Logistics.JawZ.position,
                    "speed": _s.Logistics.JawZ.speed,
                    "acc": _s.Logistics.JawZ.acc,
                    "dec": _s.Logistics.JawZ.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },


        //DJPump
        fDJPump: function () {
            let _s = this;
            axios.post(_s.url + "v1/dj/pump",
                {
                    "position": _s.DJ.Pump.position,
                    "speed": _s.DJ.Pump.speed,
                    "acc": _s.DJ.Pump.acc,
                    "dec": _s.DJ.Pump.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //DJX
        fDJX: function () {
            let _s = this;
            axios.post(_s.url + "v1/dj/x",
                {
                    "position": _s.DJ.X.position,
                    "speed": _s.DJ.X.speed,
                    "acc": _s.DJ.X.acc,
                    "dec": _s.DJ.X.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //DJY
        fDJY: function () {
            let _s = this;
            axios.post(_s.url + "v1/dj/y",
                {
                    "position": _s.DJ.Y.position,
                    "speed": _s.DJ.Y.speed,
                    "acc": _s.DJ.Y.acc,
                    "dec": _s.DJ.Y.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //DJZ
        fDJZ: function () {
            let _s = this;
            axios.post(_s.url + "v1/dj/z",
                {
                    "position": _s.DJ.Z.position,
                    "speed": _s.DJ.Z.speed,
                    "acc": _s.DJ.Z.acc,
                    "dec": _s.DJ.Z.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //QPCR光学轴初始化
        fQPCRFilterInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/qpcr/optical_filter",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("QPCR.Filter")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //QPCR金属浴X轴初始化
        fQPCRBathXInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/qpcr/bath_x",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("QPCR.BathX")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //QPCR金属浴Z轴初始化
        fQPCRBathZInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/qpcr/bath_z",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("QPCR.BathZ")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //QPCR滤光片
        fQPCRFilter: function () {
            let _s = this;
            axios.post(_s.url + "v1/qpcr/optical_filter",
                {
                    "position": _s.QPCR.Filter.position,
                    "speed": _s.QPCR.Filter.speed,
                    "acc": _s.QPCR.Filter.acc,
                    "dec": _s.QPCR.Filter.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //QPCR金属浴X
        fQPCRBathX: function () {
            let _s = this;
            axios.post(_s.url + "v1/qpcr/bath_x",
                {
                    "position": _s.QPCR.BathX.position,
                    "speed": _s.QPCR.BathX.speed,
                    "acc": _s.QPCR.BathX.acc,
                    "dec": _s.QPCR.BathX.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //QPCR金属浴Z
        fQPCRBathZ: function () {
            let _s = this;
            axios.post(_s.url + "v1/qpcr/bath_z",
                {
                    "position": _s.QPCR.BathZ.position,
                    "speed": _s.QPCR.BathZ.speed,
                    "acc": _s.QPCR.BathZ.acc,
                    "dec": _s.QPCR.BathZ.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },

        //DP8 运动至目标位置
        fDP8TarSwitch: function (tar) {
            let _s = this;
            switch (tar) {
                case 1:
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[1].x;
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[1].y;
                    _s.DP8XYZTar.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[1].z;
                    break;
                case 2:
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[2].x;
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[2].y;
                    _s.DP8XYZTar.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[2].z;
                    break;
                case 3:
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[3].x;
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[3].y;
                    _s.DP8XYZTar.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[3].z;
                    break;
                case 4:
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[4].x;
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[4].y;
                    _s.DP8XYZTar.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[4].z;
                    break;
                case 5:
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[5].x;
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[5].y;
                    _s.DP8XYZTar.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[5].z;
                    break;
                case 6:
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[6].x;
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[6].y;
                    _s.DP8XYZTar.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[6].z;
                    break;
                case 7:
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[7].x;
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[7].y;
                    _s.DP8XYZTar.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[7].z;
                    break;
                case 8:
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[8].x;
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[8].y;
                    _s.DP8XYZTar.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[8].z;
                    break;
                case 9:
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[9].x;
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[9].y;
                    _s.DP8XYZTar.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[9].z;
                    break;
                case 10:
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[10].x;
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[10].y;
                    _s.DP8XYZTar.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[10].z;
                    break;
                case 11:
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[11].x;
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[11].y;
                    _s.DP8XYZTar.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[11].z;
                    break;
                case 12:
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[12].x;
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[12].y;
                    _s.DP8XYZTar.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[12].z;
                    break;
                case 13:
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[13].x;
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[13].y;
                    _s.DP8XYZTar.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[13].z;
                    break;
                case 14:
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[14].x;
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[14].y;
                    _s.DP8XYZTar.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[14].z;
                    break;
                case 15:
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[15].x;
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[15].y;
                    _s.DP8XYZTar.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[15].z;
                    break;
                case 16:
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[16].x;
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[16].y;
                    _s.DP8XYZTar.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[16].z;
                    break;
                case 17:
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[17].x;
                    _s.DP8XYZTar.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[17].y;
                    _s.DP8XYZTar.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[17].z;
                    break;
                default:
                    alert("DP8错误目标");
                    break
            }
        },
        fDP8Move_Tar: function () {
            let _s = this;
            _s.fDP8TarSwitch(Number(_s.DP8XYZTar.tarNum));
            console.log(_s.DP8XYZMove.config.dp8);
            var targetConfig = JSON.stringify(_s.DP8XYZTar.config);
            console.log("target:" + _s.DP8XYZTar.tarNum + ",x:" + _s.DP8XYZTar.config.dp8.loc_horizen.electl[0].position + ",y:" + _s.DP8XYZTar.config.dp8.loc_horizen.electl[1].position + ",z:" + _s.DP8XYZTar.config.dp8.loc_horizen.electl[0].position);
            axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 5,
                "filesize": targetConfig.length,
                "chunk": targetConfig
            })
                .then(function (res) {
                    axios.post(_s.url + "/v1/module/DP8_MOVE", {})
                        .then(function (res) {
                            if (res.data.errorNum == 100) {
                                console.log("动作成功")
                            } else {
                                alert(res)
                            }
                        })
                        .catch(function (err) {
                            alert(err)
                        })


                })
                .catch(function (err) {
                    alert(err);
                })
        },


        //DP8移动至目标位置并吸液
        fDP8MoveAndSuck: function () {
            let _s = this;
            switch (Number(_s.DP8suck.tarNum)) {
                case 1:
                    _s.DP8suck.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[1].x;
                    _s.DP8suck.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[1].y;
                    _s.DP8suck.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[1].z;
                    break;
                case 2:
                    _s.DP8suck.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[2].x;
                    _s.DP8suck.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[2].y;
                    _s.DP8suck.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[2].z;
                    break;
                case 3:
                    _s.DP8suck.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[3].x;
                    _s.DP8suck.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[3].y;
                    _s.DP8suck.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[3].z;
                    break;
                case 4:
                    _s.DP8suck.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[4].x;
                    _s.DP8suck.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[4].y;
                    _s.DP8suck.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[4].z;
                    break;
                case 5:
                    _s.DP8suck.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[5].x;
                    _s.DP8suck.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[5].y;
                    _s.DP8suck.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[5].z;
                    break;
                case 6:
                    _s.DP8suck.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[6].x;
                    _s.DP8suck.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[6].y;
                    _s.DP8suck.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[6].z;
                    break;
                case 7:
                    _s.DP8suck.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[7].x;
                    _s.DP8suck.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[7].y;
                    _s.DP8suck.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[7].z;
                    break;
                case 8:
                    _s.DP8suck.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[8].x;
                    _s.DP8suck.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[8].y;
                    _s.DP8suck.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[8].z;
                    break;
                case 9:
                    _s.DP8suck.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[9].x;
                    _s.DP8suck.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[9].y;
                    _s.DP8suck.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[9].z;
                    break;
                case 10:
                    _s.DP8suck.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[10].x;
                    _s.DP8suck.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[10].y;
                    _s.DP8suck.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[10].z;
                    break;
                case 11:
                    _s.DP8suck.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[11].x;
                    _s.DP8suck.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[11].y;
                    _s.DP8suck.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[11].z;
                    break;
                case 12:
                    _s.DP8suck.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[12].x;
                    _s.DP8suck.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[12].y;
                    _s.DP8suck.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[12].z;
                    break;
                case 13:
                    _s.DP8suck.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[13].x;
                    _s.DP8suck.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[13].y;
                    _s.DP8suck.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[13].z;
                    break;
                case 14:
                    _s.DP8suck.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[14].x;
                    _s.DP8suck.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[14].y;
                    _s.DP8suck.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[14].z;
                    break;
                case 15:
                    _s.DP8suck.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[15].x;
                    _s.DP8suck.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[15].y;
                    _s.DP8suck.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[15].z;
                    break;
                case 16:
                    _s.DP8suck.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[16].x;
                    _s.DP8suck.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[16].y;
                    _s.DP8suck.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[16].z;
                    break;
                case 17:
                    _s.DP8suck.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[17].x;
                    _s.DP8suck.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[17].y;
                    _s.DP8suck.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[17].z;
                    break;
                default:
                    alert("DP8错误目标");
                    break
            }
            var targetConfig = JSON.stringify(_s.DP8suck.config);
            axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 5,
                "filesize": targetConfig.length,
                "chunk": targetConfig
            })
                .then(function (res) {
                    axios.post(_s.url + "/v1/module/DP8_MOVE", {})
                        .then(function (res) {
                            if (res.data.errorNum == 100) {
                                setTimeout(function () {
                                    axios.post(_s.url + "/v1/module/DP8_SUCK", {})
                                        .then(function (res) {
                                            if (res.data.errorNum == 100) {
                                                console.log("移动并吸液成功")
                                            } else {
                                                alert("移动吸液，吸液失败。" + res)
                                            }
                                        })
                                        .catch(function (err) {
                                            alert("移动吸液，吸液失败。" + err);
                                        })

                                }, 500)
                            } else {
                                alert(alert("移动吸液，移动失败。" + res))
                            }
                        })
                        .catch(function (err) {
                            alert("移动吸液，移动失败。" + err)
                        })

                })
                .catch(function (err) {
                    alert(err)
                })


        },
        //DP8移动至目标位置并喷液
        fDP8MoveAndDrop: function () {
            let _s = this;
            switch (Number(_s.DP8drop.tarNum)) {
                case 1:
                    _s.DP8drop.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[1].x;
                    _s.DP8drop.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[1].y;
                    _s.DP8drop.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[1].z;
                    break;
                case 2:
                    _s.DP8drop.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[2].x;
                    _s.DP8drop.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[2].y;
                    _s.DP8drop.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[2].z;
                    break;
                case 3:
                    _s.DP8drop.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[3].x;
                    _s.DP8drop.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[3].y;
                    _s.DP8drop.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[3].z;
                    break;
                case 4:
                    _s.DP8drop.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[4].x;
                    _s.DP8drop.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[4].y;
                    _s.DP8drop.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[4].z;
                    break;
                case 5:
                    _s.DP8drop.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[5].x;
                    _s.DP8drop.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[5].y;
                    _s.DP8drop.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[5].z;
                    break;
                case 6:
                    _s.DP8drop.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[6].x;
                    _s.DP8drop.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[6].y;
                    _s.DP8drop.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[6].z;
                    break;
                case 7:
                    _s.DP8drop.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[7].x;
                    _s.DP8drop.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[7].y;
                    _s.DP8drop.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[7].z;
                    break;
                case 8:
                    _s.DP8drop.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[8].x;
                    _s.DP8drop.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[8].y;
                    _s.DP8drop.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[8].z;
                    break;
                case 9:
                    _s.DP8drop.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[9].x;
                    _s.DP8drop.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[9].y;
                    _s.DP8drop.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[9].z;
                    break;
                case 10:
                    _s.DP8drop.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[10].x;
                    _s.DP8drop.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[10].y;
                    _s.DP8drop.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[10].z;
                    break;
                case 11:
                    _s.DP8drop.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[11].x;
                    _s.DP8drop.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[11].y;
                    _s.DP8drop.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[11].z;
                    break;
                case 12:
                    _s.DP8drop.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[12].x;
                    _s.DP8drop.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[12].y;
                    _s.DP8drop.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[12].z;
                    break;
                case 13:
                    _s.DP8drop.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[13].x;
                    _s.DP8drop.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[13].y;
                    _s.DP8drop.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[13].z;
                    break;
                case 14:
                    _s.DP8drop.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[14].x;
                    _s.DP8drop.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[14].y;
                    _s.DP8drop.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[14].z;
                    break;
                case 15:
                    _s.DP8drop.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[15].x;
                    _s.DP8drop.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[15].y;
                    _s.DP8drop.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[15].z;
                    break;
                case 16:
                    _s.DP8drop.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[16].x;
                    _s.DP8drop.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[16].y;
                    _s.DP8drop.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[16].z;
                    break;
                case 17:
                    _s.DP8drop.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[17].x;
                    _s.DP8drop.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[17].y;
                    _s.DP8drop.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[17].z;
                    break;
                default:
                    alert("DP8错误目标");
                    break
            }
            var targetConfig = JSON.stringify(_s.DP8drop.config);
            axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 5,
                "filesize": targetConfig.length,
                "chunk": targetConfig
            })
                .then(function (res) {
                    axios.post(_s.url + "/v1/module/DP8_MOVE", {})
                        .then(function (res) {
                            if (res.data.errorNum == 100) {
                                setTimeout(function () {
                                    axios.post(_s.url + "/v1/module/DP8_SPIT", {})
                                        .then(function (res) {
                                            if (res.data.errorNum == 100) {
                                                console.log("移动并喷液成功")
                                            } else {
                                                alert("移动喷液，喷液失败。" + res)
                                            }
                                        })
                                        .catch(function (err) {
                                            alert("移动喷液，喷液失败。" + err);
                                        })

                                }, 500)
                            } else {
                                alert(alert("移动喷液，移动失败。" + res))
                            }
                        })
                        .catch(function (err) {
                            alert("移动喷液，移动失败。" + err)
                        })

                })
                .catch(function (err) {
                    alert(err)
                })


        },

        //移动并取TIP
        fDP8MoveAndTipOn:function(){
            let _s = this;
            switch (Number(_s.DP8TipOn.tarNum)) {
                case 1:
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[1].x;
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[1].y;
                    _s.DP8TipOn.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[1].z;
                    break;
                case 2:
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[2].x;
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[2].y;
                    _s.DP8TipOn.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[2].z;
                    break;
                case 3:
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[3].x;
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[3].y;
                    _s.DP8TipOn.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[3].z;
                    break;
                case 4:
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[4].x;
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[4].y;
                    _s.DP8TipOn.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[4].z;
                    break;
                case 5:
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[5].x;
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[5].y;
                    _s.DP8TipOn.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[5].z;
                    break;
                case 6:
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[6].x;
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[6].y;
                    _s.DP8TipOn.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[6].z;
                    break;
                case 7:
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[7].x;
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[7].y;
                    _s.DP8TipOn.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[7].z;
                    break;
                case 8:
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[8].x;
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[8].y;
                    _s.DP8TipOn.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[8].z;
                    break;
                case 9:
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[9].x;
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[9].y;
                    _s.DP8TipOn.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[9].z;
                    break;
                case 10:
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[10].x;
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[10].y;
                    _s.DP8TipOn.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[10].z;
                    break;
                case 11:
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[11].x;
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[11].y;
                    _s.DP8TipOn.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[11].z;
                    break;
                case 12:
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[12].x;
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[12].y;
                    _s.DP8TipOn.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[12].z;
                    break;
                case 13:
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[13].x;
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[13].y;
                    _s.DP8TipOn.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[13].z;
                    break;
                case 14:
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[14].x;
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[14].y;
                    _s.DP8TipOn.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[14].z;
                    break;
                case 15:
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[15].x;
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[15].y;
                    _s.DP8TipOn.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[15].z;
                    break;
                case 16:
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[16].x;
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[16].y;
                    _s.DP8TipOn.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[16].z;
                    break;
                case 17:
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[17].x;
                    _s.DP8TipOn.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[17].y;
                    _s.DP8TipOn.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[17].z;
                    break;
                default:
                    alert("DP8错误目标");
                    break
            }
            var targetConfig = JSON.stringify(_s.DP8TipOn.config);
            axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 5,
                "filesize": targetConfig.length,
                "chunk": targetConfig
            })
                .then(function (res) {
                    axios.post(_s.url + "/v1/module/DP8_MOVE", {})
                        .then(function (res) {
                            if (res.data.errorNum == 100) {
                                setTimeout(function () {
                                    axios.post(_s.url + "/v1/module/DP8_TipOn", {})
                                        .then(function (res) {
                                            if (res.data.errorNum == 100) {
                                                console.log("移动并取头成功")
                                            } else {
                                                alert("移动取头，取头失败。" + res)
                                            }
                                        })
                                        .catch(function (err) {
                                            alert("移动取头，取头失败。" + err);
                                        })

                                }, 500)
                            } else {
                                alert(alert("移动取头，移动失败。" + res))
                            }
                        })
                        .catch(function (err) {
                            alert("移动取头，移动失败。" + err)
                        })

                })
                .catch(function (err) {
                    alert(err)
                })


        },
        fDP8MoveAndTipDown:function(){
            let _s = this;
            switch (Number(_s.DP8TipDown.tarNum)) {
                case 1:
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[1].x;
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[1].y;
                    _s.DP8TipDown.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[1].z;
                    break;
                case 2:
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[2].x;
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[2].y;
                    _s.DP8TipDown.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[2].z;
                    break;
                case 3:
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[3].x;
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[3].y;
                    _s.DP8TipDown.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[3].z;
                    break;
                case 4:
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[4].x;
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[4].y;
                    _s.DP8TipDown.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[4].z;
                    break;
                case 5:
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[5].x;
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[5].y;
                    _s.DP8TipDown.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[5].z;
                    break;
                case 6:
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[6].x;
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[6].y;
                    _s.DP8TipDown.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[6].z;
                    break;
                case 7:
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[7].x;
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[7].y;
                    _s.DP8TipDown.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[7].z;
                    break;
                case 8:
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[8].x;
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[8].y;
                    _s.DP8TipDown.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[8].z;
                    break;
                case 9:
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[9].x;
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[9].y;
                    _s.DP8TipDown.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[9].z;
                    break;
                case 10:
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[10].x;
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[10].y;
                    _s.DP8TipDown.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[10].z;
                    break;
                case 11:
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[11].x;
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[11].y;
                    _s.DP8TipDown.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[11].z;
                    break;
                case 12:
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[12].x;
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[12].y;
                    _s.DP8TipDown.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[12].z;
                    break;
                case 13:
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[13].x;
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[13].y;
                    _s.DP8TipDown.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[13].z;
                    break;
                case 14:
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[14].x;
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[14].y;
                    _s.DP8TipDown.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[14].z;
                    break;
                case 15:
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[15].x;
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[15].y;
                    _s.DP8TipDown.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[15].z;
                    break;
                case 16:
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[16].x;
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[16].y;
                    _s.DP8TipDown.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[16].z;
                    break;
                case 17:
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZTar.tarXYZ[17].x;
                    _s.DP8TipDown.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZTar.tarXYZ[17].y;
                    _s.DP8TipDown.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZTar.tarXYZ[17].z;
                    break;
                default:
                    alert("DP8错误目标");
                    break
            }
            var targetConfig = JSON.stringify(_s.DP8TipDown.config);
            axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 5,
                "filesize": targetConfig.length,
                "chunk": targetConfig
            })
                .then(function (res) {
                    axios.post(_s.url + "/v1/module/DP8_MOVE", {})
                        .then(function (res) {
                            if (res.data.errorNum == 100) {
                                setTimeout(function () {
                                    axios.post(_s.url + "/v1/module/DP8_TipDown", {})
                                        .then(function (res) {
                                            if (res.data.errorNum == 100) {
                                                console.log("移动并退头成功")
                                            } else {
                                                alert("移动退头，退头失败。" + res)
                                            }
                                        })
                                        .catch(function (err) {
                                            alert("移动退头，退头失败。" + err);
                                        })

                                }, 500)
                            } else {
                                alert(alert("移动退头，移动失败。" + res))
                            }
                        })
                        .catch(function (err) {
                            alert("移动退头，移动失败。" + err)
                        })

                })
                .catch(function (err) {
                    alert(err)
                })


        },


        fDP8Move: function () {
            let _s = this;

            _s.DP8XYZMove.config.dp8.loc_horizen.electl[0].position = _s.DP8XYZMove.x;
            _s.DP8XYZMove.config.dp8.loc_horizen.electl[1].position = _s.DP8XYZMove.y;
            _s.DP8XYZMove.config.dp8.loc_vertical.electl[0].position = _s.DP8XYZMove.z;
            console.log("x:" + _s.DP8XYZMove.config.dp8.loc_horizen.electl[0].position + "y:" + _s.DP8XYZMove.config.dp8.loc_horizen.electl[1].position + "z:" + _s.DP8XYZMove.config.dp8.loc_vertical.electl[0].position);
            var targetConfig = JSON.stringify(_s.DP8XYZMove.config);
            axios.post(_s.url + "/v1/param/to_board", {
                "fileId": 5,
                "filesize": targetConfig.length,
                "chunk": targetConfig
            })
                .then(function (res) {
                    axios.post(_s.url + "/v1/module/DP8_MOVE", {})
                        .then(function (res) {
                            if (res.data.errorNum == 100) {
                                console.log("动作成功")
                            } else {
                                alert(res)
                            }
                        })
                        .catch(function (err) {
                            alert(err)
                        })


                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //DP8取吸头
        fDP8TipOn: function () {
            let _s = this;
            axios.post(_s.url + "/v1/module/DP8_TipOn", {}).then(function (res) {
                console.log(res);
            })
                .catch(function (err) {
                    alert(err)
                })
        },
        //DP8退吸头
        fDP8TipDown: function () {
            let _s = this;
            axios.post(_s.url + "/v1/module/DP8_TipDown", {}).then(function (res) {
                console.log(res);
            })
                .catch(function (err) {
                    alert(err)
                })
        },
        //DP8吸液
        fDP8Suck: function () {
            let _s = this;
            axios.post(_s.url + "/v1/module/DP8_SUCK", {}).then(function (res) {
                console.log(res);
            })
                .catch(function (err) {
                    alert(err)
                })
        },
        //DP8喷液
        fDP8SPIT: function () {
            let _s = this;
            axios.post(_s.url + "/v1/module/DP8_SPIT", {}).then(function (res) {
                console.log(res);
            })
                .catch(function (err) {
                    alert(err)
                })
        },


    }


});

