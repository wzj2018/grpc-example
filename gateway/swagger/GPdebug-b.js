//tab标签实现
window.onload = function () {
    var title = document.getElementById("Navigation-item").getElementsByTagName("li");
    var page = document.getElementById("Operation-item").getElementsByTagName("li");
    for (var i = 0, len = title.length; i < len; i++) {
        title[i].id = i;
        title[i].onclick = function () {
            for (j = 0; j < len; j++) {
                title[j].classList.remove("selected");
                page[j].classList.remove("selected");

            }
            this.className = "selected";
            page[this.id].className = "selected";

        }

    }
};

new Vue({
    el: "#app",
    data: {
        // qpcr: {
        //     Preceding: "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
        //         "<rdml xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" version=\"1.1\" xmlns=\"http://www.rdml.org\">\n" +
        //         "  <dye id=\"FAM\" />\n" +
        //         "  <dye id=\"HEX\" />\n" +
        //         "  <dye id=\"ROX\" />\n" +
        //         "  <dye id=\"AHH\" />\n" +
        //         "  <dye id=\"OHH\" />\n" +
        //         "  <dye id=\"VIC\" />\n" +
        //         "  <target id=\"FAM@1\">\n" +
        //         "    <type>ref</type>\n" +
        //         "    <amplificationEfficiency>0</amplificationEfficiency>\n" +
        //         "    <dyeId id=\"FAM\" />\n" +
        //         "  </target>\n" +
        //         "  <target id=\"HEX@2\">\n" +
        //         "    <type>ref</type>\n" +
        //         "    <amplificationEfficiency>0</amplificationEfficiency>\n" +
        //         "    <dyeId id=\"HEX\" />\n" +
        //         "  </target>\n" +
        //         "  <target id=\"ROX@3\">\n" +
        //         "    <type>ref</type>\n" +
        //         "    <amplificationEfficiency>0</amplificationEfficiency>\n" +
        //         "    <dyeId id=\"ROX\" />\n" +
        //         "  </target>\n" +
        //         "  <target id=\"VIC@4\">\n" +
        //         "    <type>ref</type>\n" +
        //         "    <amplificationEfficiency>0</amplificationEfficiency>\n" +
        //         "    <dyeId id=\"VIC\" />\n" +
        //         "  </target>\n" +
        //         "  <target id=\"AHH@5\">\n" +
        //         "    <type>ref</type>\n" +
        //         "    <dyeId id=\"AHH\" />\n" +
        //         "  </target>\n" +
        //         "  <target id=\"OHH@6\">\n" +
        //         "    <type>ref</type>\n" +
        //         "    <dyeId id=\"OHH\" />\n" +
        //         "  </target>\n" +
        //         "  <target id=\"FAM@Y\">\n" +
        //         "    <type>ref</type>\n" +
        //         "    <dyeId id=\"FAM\" />\n" +
        //         "  </target>\n" +
        //         "  <target id=\"HEX@IPC\">\n" +
        //         "    <type>ref</type>\n" +
        //         "    <dyeId id=\"HEX\" />\n" +
        //         "  </target>\n" +
        //         "  <thermalCyclingConditions id=\"1234\">\n" +
        //         "    <step>\n" +
        //         "      <nr>1</nr>\n" +
        //         "      <temperature>\n" +
        //         "        <temperature>95</temperature>\n" +
        //         "        <duration>5</duration>\n" +
        //         "        <ramp>4.4</ramp>\n" +
        //         "      </temperature>\n" +
        //         "    </step>\n" +
        //         "    <step>\n" +
        //         "      <nr>2</nr>\n" +
        //         "      <temperature>\n" +
        //         "        <temperature>61</temperature>\n" +
        //         "        <duration>5</duration>\n" +
        //         "        <ramp>2.2</ramp>\n" +
        //         "      </temperature>\n" +
        //         "    </step>\n" +
        //         "    <step>\n" +
        //         "      <nr>3</nr>\n" +
        //         "      <temperature>\n" +
        //         "        <temperature>68</temperature>\n" +
        //         "        <duration>25</duration>\n" +
        //         "        <measure>real time</measure>\n" +
        //         "        <ramp>4.4</ramp>\n" +
        //         "      </temperature>\n" +
        //         "    </step>\n" +
        //         "    <step>\n" +
        //         "      <nr>4</nr>\n" +
        //         "      <loop>\n" +
        //         "        <goto>2</goto>\n" +
        //         "        <repeat>",
        //     CyclerTime: 0,
        //     Post: "</repeat>\n" +
        //         "</loop>\n" +
        //         "</step>\n" +
        //         "  </thermalCyclingConditions>\n" +
        //         "    <experiment id=\"Human Test experiment_2019_03_14 15:48:15\">\n" +
        //         "        <run id=\"run_id_000001\">\n" +
        //         "            <thermalCyclingConditions id=\"1234\" />\n" +
        //         "            <pcrFormat>\n" +
        //         "                <rows>6</rows>\n" +
        //         "                <columns>8</columns>\n" +
        //         "                <rowLabel>ABC</rowLabel>\n" +
        //         "                <columnLabel>123</columnLabel>\n" +
        //         "            </pcrFormat>\n" +
        //         "        </run>\n" +
        //         "   </experiment>\n" +
        //         "</rdml>",
        //
        // }
        url: "http://10.6.3.74:8888",
        openLid: {
            Y: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0,
            },
            jaw: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            Z: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            Echo: {
                flag: false,
                Step: ""
            }
        },
        ScanCode: {
            position: 0,
            speed: 0,
            acc: 0,
            dec: 0

        },
        XR: {
            Y: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0,
            },
            jaw: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            Z: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            rotate: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
        },
        DP8: {
            X: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            Y: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            Z: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            suck: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            }


        },
        Seal: {

            Z: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            }


        },
        DP1: {
            Jaw: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            JawZ: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            X: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            Y: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },

        },
        DJ: {
            X: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            Y: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            Z: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            Pump: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },

        },
        QPCR: {
            Filter: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            BathX: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            BathZ: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
        },

        Logistics: {
            X: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            JawY: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            JawZ: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },
            Jaw: {
                position: 0,
                speed: 0,
                acc: 0,
                dec: 0
            },

        },
        InitSet: {
            pos: 0,
            speed: 8000,
            acc: 10000,
            dec: 10000,

        }
    },
    methods: {
        // qpcrStart: function () {
        //     let _s = this;
        //     axios.put("http://10.6.3.74:8082/v1/rdml/run_rdml", _s.qpcr.Preceding + _s.qpcr.CyclerTime + _s.qpcr.Post,{headers: {'Content-Type': 'application/json'}})
        //         .then(function (res) {
        //             if (res.data.Status == "success") {
        //                 console.log("成了，次数" + _s.qpcr.CyclerTime);
        //             } else {
        //                 console.log("发错东西了")
        //             }
        //         })
        //         .catch(function (err) {
        //             alert(err);
        //         })
        //
        // }


        fResJudge: function (res) {
            switch (res.data.errorNum) {
                case 100:
                    console.log("动作成功");
                    break;
                // case 1:
                //     switch (res.data.errorNum) {
                //         case 1:
                //             alert("超出行程");
                //             break;
                //         case 2:
                //             alert("超时");
                //             break;
                //         case 3:
                //             alert("控制板信息错误");
                //             break;
                //     }
                case 2:
                    alert("动作超时");
                    break;
                default:
                    alert("动作出错");
            }
        },

        fchangeState: function (tar) {
            tag = document.getElementById(tar);
            tag.classList.remove("Uninited");
            tag.classList.add("Inited");
        },

        fDP8XInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/dp8/x",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DP8.X")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        fDP8YInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/dp8/y",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DP8.Y")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        fDP8ZInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/dp8/z",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DP8.Z")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        fDP8suckInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/dp8/suck_liquid",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DP8.suck")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },


        fSealZInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/thermal_seal/z",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("Seal.Z")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },

        fOpenLidZInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/openLid/z",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("openLid.Z")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },


        fOpenLidYInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/openLid/y",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("openLid.Y")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        fOpenLidjawInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/openLid/jaw",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("openLid.jaw")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //DP1夹爪初始化
        fDP1JawInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/dp1/drop_tube_draw",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DP1.Jaw")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //DP1夹爪Z初始化
        fDP1JawZInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/dp1/jaw_z",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DP1.JawZ")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //DP1X初始化
        fDP1XInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/dp1/x",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DP1.X")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //DP1Y初始化
        fDP1YInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/dp1/y",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DP1.Y")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },


        fXRZInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/xrModule/z",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("XR.Z")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        fXRYInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/xrModule/y",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("XR.Z")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        fXRjawInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/xrModule/jaw",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("XR.Z")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        fXRrotateInit: function () {
            let _s = this;
            axios.post(_s.url + "/v1/xrModule/rotate",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("XR.rotate")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },


        fOpenLidY: function () {
            let _s = this;
            axios.post(_s.url + "/v1/openLid/y",
                {
                    "position": _s.openLid.Y.position,
                    "speed": _s.openLid.Y.speed,
                    "acc": _s.openLid.Y.acc,
                    "dec": _s.openLid.Y.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })


        },
        fOpenLidjaw: function () {
            let _s = this;
            axios.post(_s.url + "/v1/openLid/jaw",
                {
                    "position": _s.openLid.jaw.position,
                    "speed": _s.openLid.jaw.speed,
                    "acc": _s.openLid.jaw.acc,
                    "dec": _s.openLid.jaw.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })


        },
        fOpenLidZ: function () {
            let _s = this;
            axios.post(_s.url + "/v1/openLid/z",
                {
                    "position": _s.openLid.Z.position,
                    "speed": _s.openLid.Z.speed,
                    "acc": _s.openLid.Z.acc,
                    "dec": _s.openLid.Z.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        fScanCode: function () {
            let _s = this;
            axios.post(_s.url + "/v1/scan_mode/rotate",
                {
                    "position": _s.ScanCode.position,
                    "speed": _s.ScanCode.speed,
                    "acc": _s.ScanCode.acc,
                    "dec": _s.ScanCode.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        fXRZ: function () {
            let _s = this;
            axios.post(_s.url + "/v1/xrModule/z",
                {
                    "position": _s.XR.Z.position,
                    "speed": _s.XR.Z.speed,
                    "acc": _s.XR.Z.acc,
                    "dec": _s.XR.Z.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        fXRY: function () {
            let _s = this;
            axios.post(_s.url + "/v1/xrModule/y",
                {
                    "position": _s.XR.Y.position,
                    "speed": _s.XR.Y.speed,
                    "acc": _s.XR.Y.acc,
                    "dec": _s.XR.Y.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },

        fXRjaw: function () {
            let _s = this;
            axios.post(_s.url + "/v1/xrModule/jaw",
                {
                    "position": _s.XR.jaw.position,
                    "speed": _s.XR.jaw.speed,
                    "acc": _s.XR.jaw.acc,
                    "dec": _s.XR.jaw.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },

        fXRrotate: function () {
            let _s = this;
            axios.post(_s.url + "/v1/xrModule/rotate",
                {
                    "position": _s.XR.rotate.position,
                    "speed": _s.XR.rotate.speed,
                    "acc": _s.XR.rotate.acc,
                    "dec": _s.XR.rotate.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },


        fDP8X: function () {
            let _s = this;
            axios.post(_s.url + "/v1/dp8/x",
                {
                    "position": _s.DP8.X.position,
                    "speed": _s.DP8.X.speed,
                    "acc": _s.DP8.X.acc,
                    "dec": _s.DP8.X.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);


                })
                .catch(function (err) {
                    alert(err);
                })

        },
        fDP8Y: function () {
            let _s = this;
            axios.post(_s.url + "/v1/dp8/y",
                {
                    "position": _s.DP8.Y.position,
                    "speed": _s.DP8.Y.speed,
                    "acc": _s.DP8.Y.acc,
                    "dec": _s.DP8.Y.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        fDP8Z: function () {
            let _s = this;
            axios.post(_s.url + "/v1/dp8/z",
                {
                    "position": _s.DP8.Z.position,
                    "speed": _s.DP8.Z.speed,
                    "acc": _s.DP8.Z.acc,
                    "dec": _s.DP8.Z.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },


        fDP8suck: function () {
            let _s = this;
            axios.post(_s.url + "/v1/dp8/suck_liquid",
                {
                    "position": _s.DP8.suck.position,
                    "speed": _s.DP8.suck.speed,
                    "acc": _s.DP8.suck.acc,
                    "dec": _s.DP8.suck.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },

        fSealZ: function () {
            let _s = this;
            axios.post(_s.url + "/v1/thermal_seal/z",
                {
                    "position": _s.Seal.Z.position,
                    "speed": _s.Seal.Z.speed,
                    "acc": _s.Seal.Z.acc,
                    "dec": _s.Seal.Z.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //DP1夹爪
        fDP1Jaw: function () {
            let _s = this;
            axios.post(_s.url + "/v1/dp1/drop_Jaw_draw",
                {
                    "position": _s.DP1.Jaw.position,
                    "speed": _s.DP1.Jaw.speed,
                    "acc": _s.DP1.Jaw.acc,
                    "dec": _s.DP1.Jaw.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //DP1夹爪Z
        fDP1JawZ: function () {
            let _s = this;
            axios.post(_s.url + "v1/dp1/jaw_z",
                {
                    "position": _s.DP1.JawZ.position,
                    "speed": _s.DP1.JawZ.speed,
                    "acc": _s.DP1.JawZ.acc,
                    "dec": _s.DP1.JawZ.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //DP1X轴
        fDP1X: function () {
            let _s = this;
            axios.post(_s.url + "v1/dp1/x",
                {
                    "position": _s.DP1.X.position,
                    "speed": _s.DP1.X.speed,
                    "acc": _s.DP1.X.acc,
                    "dec": _s.DP1.X.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //DP1Y轴
        fDP1Y: function () {
            let _s = this;
            axios.post(_s.url + "v1/dp1/y",
                {
                    "position": _s.DP1.Y.position,
                    "speed": _s.DP1.Y.speed,
                    "acc": _s.DP1.Y.acc,
                    "dec": _s.DP1.Y.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },


        //开盖
        fOpenLid: function () {
            let _s = this;
            axios.post(_s.url + "/v1/module/OpenLid",
                {})
                .then(function (res) {
                    console.log(res);
                })
                .catch(function (err) {
                    alert(err)
                })


        },


        //关盖
        fCloseLid: function () {
            let _s = this;
            axios.post(_s.url + "/v1/module/CloseLid",
                {})
                .then(function (res) {
                    console.log(res);
                })
                .catch(function (err) {
                    alert(err)
                })


        },
        //AtoB
        fAtoB: function () {
            let _s = this;
            axios.post(_s.url + "/v1/module/XR_AToB",
                {})
                .then(function (res) {
                    console.log(res);
                })
                .catch(function (err) {
                    alert(err)
                })
        },


        //开盖、关盖循环
        EchoOpenLidStart: function () {
            let _s = this;
            _s.openLid.Echo.flag = true;
            _s.EchoOpenLid();
        },


        EchoOpenLid: function () {
            let _s = this;
            if (_s.openLid.Echo.flag == true) {
                axios.post(_s.url + "/v1/module/OpenLid",
                    {})
                    .then(function (res) {
                        console.log(res);
                        if (res.data.StateCode == null) {
                            _s.openLid.Echo.Step = "Close";
                            setTimeout(function () {
                                _s.EchoCloseLid();
                            }, 5000);
                        } else {
                            alert(res.data)
                        }

                    })
                    .catch(function (err) {
                        alert(err)
                    })
            }
        },
        EchoCloseLid: function () {
            let _s = this;
            if (_s.openLid.Echo.flag == true) {
                axios.post(_s.url + "/v1/module/CloseLid",
                    {})
                    .then(function (res) {
                        console.log(res);
                        if (res.data.StateCode == null) {
                            _s.openLid.Echo.Step = "Open";
                            setTimeout(function () {
                                _s.EchoOpenLid();
                            }, 5000);
                        } else {
                            alert(res.data)
                        }
                    })
                    .catch(function (err) {
                        alert(err)
                    })
            }
        },
        EchoOpenLidPause: function () {
            this.openLid.Echo.flag = false;
        },
        EchoOpenLidGoOn: function () {
            let _s = this;
            _s.openLid.Echo.flag = true;
            switch (_s.openLid.Echo.Step) {
                case "Close":
                    _s.EchoCloseLid();
                    break;
                case "Open":
                    _s.EchoOpenLid();
                    break;
                default:
                    console.log("GoOn函数错了");
            }

        },
        //DJ柱塞泵初始化
        fDJPumpInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/dj/pump",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DJ.Pump")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //DJX初始化
        fDJXInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/dj/x",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DJ.X")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //DJY初始化
        fDJYInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/dj/y",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DJ.Y")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },

        //DJZ初始化
        fDJZInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/dj/z",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("DJ.Z")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },

        //物流X初始化
        fLogisticsXInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/logistics/x",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("Logistics.X")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //物流夹爪初始化
        fLogisticsJawInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/logistics/jaw",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("Logistics.Jaw")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //物流夹爪Y初始化
        fLogisticsJawYInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/logistics/jaw_y",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("Logistics.JawY")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //物流夹爪Z初始化
        fLogisticsJawZInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/logistics/jaw_z",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("Logistics.JawZ")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },

        //物流X
        fLogisticsX: function () {
            let _s = this;
            axios.post(_s.url + "v1/logistics/x",
                {
                    "position": _s.Logistics.X.position,
                    "speed": _s.Logistics.X.speed,
                    "acc": _s.Logistics.X.acc,
                    "dec": _s.Logistics.X.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },

        //物流夹爪
        fLogisticsJaw: function () {
            let _s = this;
            axios.post(_s.url + "v1/logistics/jaw",
                {
                    "position": _s.Logistics.Jaw.position,
                    "speed": _s.Logistics.Jaw.speed,
                    "acc": _s.Logistics.Jaw.acc,
                    "dec": _s.Logistics.Jaw.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //物流夹爪Y
        fLogisticsJawY: function () {
            let _s = this;
            axios.post(_s.url + "v1/logistics/jaw_y",
                {
                    "position": _s.Logistics.JawY.position,
                    "speed": _s.Logistics.JawY.speed,
                    "acc": _s.Logistics.JawY.acc,
                    "dec": _s.Logistics.JawY.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //物流夹爪Z
        fLogisticsJawZ: function () {
            let _s = this;
            axios.post(_s.url + "v1/logistics/jaw_z",
                {
                    "position": _s.Logistics.JawZ.position,
                    "speed": _s.Logistics.JawZ.speed,
                    "acc": _s.Logistics.JawZ.acc,
                    "dec": _s.Logistics.JawZ.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },


        //DJPump
        fDJPump: function () {
            let _s = this;
            axios.post(_s.url + "v1/dj/pump",
                {
                    "position": _s.DJ.Pump.position,
                    "speed": _s.DJ.Pump.speed,
                    "acc": _s.DJ.Pump.acc,
                    "dec": _s.DJ.Pump.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //DJX
        fDJX: function () {
            let _s = this;
            axios.post(_s.url + "v1/dj/x",
                {
                    "position": _s.DJ.X.position,
                    "speed": _s.DJ.X.speed,
                    "acc": _s.DJ.X.acc,
                    "dec": _s.DJ.X.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //DJY
        fDJY: function () {
            let _s = this;
            axios.post(_s.url + "v1/dj/y",
                {
                    "position": _s.DJ.Y.position,
                    "speed": _s.DJ.Y.speed,
                    "acc": _s.DJ.Y.acc,
                    "dec": _s.DJ.Y.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //DJZ
        fDJZ: function () {
            let _s = this;
            axios.post(_s.url + "v1/dj/z",
                {
                    "position": _s.DJ.Z.position,
                    "speed": _s.DJ.Z.speed,
                    "acc": _s.DJ.Z.acc,
                    "dec": _s.DJ.Z.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //QPCR光学轴初始化
        fQPCRFilterInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/qpcr/optical_filter",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("QPCR.Filter")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //QPCR金属浴X轴初始化
        fQPCRBathXInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/qpcr/bath_x",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("QPCR.BathX")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //QPCR金属浴Z轴初始化
        fQPCRBathZInit: function () {
            let _s = this;
            axios.post(_s.url + "v1/qpcr/bath_z",
                {
                    "position": _s.InitSet.pos,
                    "speed": _s.InitSet.speed,
                    "acc": _s.InitSet.acc,
                    "dec": _s.InitSet.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    if (res.data.errorNum == 100) {
                        _s.fchangeState("QPCR.BathZ")
                    }
                })
                .catch(function (err) {
                    alert(err);
                })
        },
        //QPCR滤光片
        fQPCRFilter: function () {
            let _s = this;
            axios.post(_s.url + "v1/qpcr/optical_filter",
                {
                    "position": _s.QPCR.Filter.position,
                    "speed": _s.QPCR.Filter.speed,
                    "acc": _s.QPCR.Filter.acc,
                    "dec": _s.QPCR.Filter.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //QPCR金属浴X
        fQPCRBathX: function () {
            let _s = this;
            axios.post(_s.url + "v1/qpcr/bath_x",
                {
                    "position": _s.QPCR.BathX.position,
                    "speed": _s.QPCR.BathX.speed,
                    "acc": _s.QPCR.BathX.acc,
                    "dec": _s.QPCR.BathX.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },
        //QPCR金属浴Z
        fQPCRBathZ: function () {
            let _s = this;
            axios.post(_s.url + "v1/qpcr/bath_z",
                {
                    "position": _s.QPCR.BathZ.position,
                    "speed": _s.QPCR.BathZ.speed,
                    "acc": _s.QPCR.BathZ.acc,
                    "dec": _s.QPCR.BathZ.dec
                }
            )
                .then(function (res) {
                    console.log(res);
                    _s.fResJudge(res);

                })
                .catch(function (err) {
                    alert(err);
                })

        },

        //DP8 XYZ联动
        fDP8Move:function(){
            let _s=this;
            axios.post(_s.url+"/v1/module/DP8_MOVE",{

            })  .then(function (res) {
                console.log(res);
            })
                .catch(function (err) {
                    alert(err)
                })
        },
        //DP8取吸头
        fDP8TipOn:function(){
            let _s=this;
            axios.post(_s.url+"/v1/module/DP8_TipOn",{

            })  .then(function (res) {
                console.log(res);
            })
                .catch(function (err) {
                    alert(err)
                })
        },
        //DP8退吸头
        fDP8TipDown:function(){
            let _s=this;
            axios.post(_s.url+"/v1/module/DP8_TipDown",{

            })  .then(function (res) {
                console.log(res);
            })
                .catch(function (err) {
                    alert(err)
                })
        },
        //DP8吸液
        fDP8Suck:function(){
            let _s=this;
            axios.post(_s.url+"/v1/module/DP8_SUCK",{

            })  .then(function (res) {
                console.log(res);
            })
                .catch(function (err) {
                    alert(err)
                })
        },
        //DP8喷液
        fDP8SPIT:function(){
            let _s=this;
            axios.post(_s.url+"/v1/module/DP8_SPIT",{

            })  .then(function (res) {
                console.log(res);
            })
                .catch(function (err) {
                    alert(err)
                })
        },
    }


});
