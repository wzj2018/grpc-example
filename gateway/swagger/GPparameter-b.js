//tab标签实现
window.onload = function () {
    var title = document.getElementById("Navigation-item").getElementsByTagName("li");
    var page = document.getElementById("Operation-item").getElementsByTagName("li");
    for (var i = 0, len = title.length; i < len; i++) {
        title[i].id = i;
        title[i].onclick = function () {
            for (j = 0; j < len; j++) {
                title[j].classList.remove("selected");
                page[j].classList.remove("selected");

            }
            this.className = "selected";
            page[this.id].className = "selected";

        }
    }


};

new Vue({
    el: "#app",
    data: {
        url: "http://10.6.3.74:8888",
        config:
            {
                //OpenLid   ID=0
                //CloseLid  ID=1
                //xr        ID=2
                //Bash      ID=3
                //Trans     ID=4
                //dp8       ID=5
                //Index     ID=6
                OpenLid: {},
                CloseLid: {},
                xr: {
                    "xr": {

                        "gotoStartY": {
                            //Y轴移动位置
                            "electl": {
                                "id": 1,
                                "ele": "xr_y",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        },
                        "startRotate": {
                            //旋转电机运动位置
                            "electl": {
                                "id": 1,
                                "ele": "xr_r",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        },
                        "startDown": {
                            //z轴开始位置
                            "electl": {
                                "id": 1,
                                "ele": "xr_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        },
                        "jawclose": {
                            //夹爪关闭参数
                            "electl": {
                                "id": 1,
                                "ele": "xr_jaw",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        },
                        "rotate": {
                            //夹取后z轴安全位置
                            "electl": [{
                                "id": 1,
                                "ele": "xr_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                                {
                                    //夹取后旋转电机目标
                                    "id": 1,
                                    "pre_wait": 0,
                                    "ele": "xr_r",
                                    "speed": 0,
                                    "acc": 0,
                                    "dec": 0,
                                    "position": 0
                                }
                            ]
                        },
                        "gotoTarY": {
                            "electl": [{
                                //Y轴目标位置
                                "id": 1,
                                "ele": "xr_y",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                                {
                                    "id": 2,
                                    "ele": "xr_r",
                                    "speed": 0,
                                    "acc": 0,
                                    "dec": 0,
                                    "position": 0
                                }
                            ]

                        },
                        "downTar": {
                            "electl": {
                                //z轴目标位置
                                "id": 1,
                                "ele": "xr_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        },
                        "jawopen": {
                            //夹爪打开参数
                            "electl": [{
                                "id": 1,
                                "ele": "xr_jaw",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                                {

                                    "id": 2,
                                    "pre_wait": 0,
                                    "ele": "xr_y",
                                    "speed": 0,
                                    "acc": 0,
                                    "dec": 0,
                                    "position": 0
                                }
                            ]
                        },
                        "goreset": {
                            "electl": [{
                                //完成后z轴位置
                                "id": 1,
                                "ele": "xr_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            },
                                {
                                    //完成后y轴位置
                                    "id": 2,
                                    "pre_wait": 0,
                                    "ele": "xr_y",
                                    "speed": 0,
                                    "acc": 0,
                                    "dec": 0,
                                    "position": 0
                                }
                            ]
                        }
                    }
                },
                Bash: {},
                Trans: {},
                dp8: {
                    "dp8": {
                        "loc_horizen": {
                            "electl": [
                                {
                                    //X轴运动
                                    "id": 1,
                                    "pre_wait": 0,
                                    "ele": "dp8_x",
                                    "speed": 0,
                                    "acc": 0,
                                    "dec": 0,
                                    "position": 0
                                },
                                {
                                    //Y轴运动
                                    "id": 2,
                                    "pre_wait": 0,
                                    "ele": "dp8_y",
                                    "speed": 0,
                                    "acc": 0,
                                    "dec": 0,
                                    "position": 0
                                },
                                {
                                    //Z轴运动安全位置
                                    "id": 3,
                                    "ele": "dp8_z",
                                    "speed": 0,
                                    "acc": 0,
                                    "dec": 0,
                                    "position": 0
                                }
                            ]
                        },
                        "loc_vertical": {
                            "electl": [
                                {
                                    //z轴运动
                                    "id": 1,
                                    "ele": "dp8_z",
                                    "speed": 0,
                                    "acc": 0,
                                    "dec": 0,
                                    "position": 0
                                },
                                {
                                    //xyz联动时吸液电机位置
                                    "id": 2,
                                    "ele": "dp8_suck",
                                    "speed": 0,
                                    "acc": 0,
                                    "dec": 0,
                                    "position": 0
                                }
                            ]
                        },
                        "probe": {
                            "electl": {
                                //液位探测极限位
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        },
                        "suck_act": {
                            "electl": [
                                {
                                    //吸液z轴相对位移
                                    "id": 1,
                                    "ele": "dp8_z",
                                    "speed": 0,
                                    "acc": 0,
                                    "dec": 0,
                                    "position": 0
                                },
                                {
                                    //吸液电机相对位移
                                    "id": 2,
                                    "ele": "dp8_suck",
                                    "speed": 0,
                                    "acc": 0,
                                    "dec": 0,
                                    "position": 0
                                }
                            ]
                        },
                        "suck_end": {
                            "electl": {
                                //吸液完成z位置
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        },
                        "droptip_down": {
                            "electl": {
                                //退tip头吸液电机下位置
                                "id": 1,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        },
                        "droptip_up": {
                            "electl": {
                                //退tip头吸液电机上位置
                                "id": 1,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        },
                        "spit_down": {
                            "electl": {
                                //喷液吸液电机下位置
                                "id": 1,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        },
                        "spit_up": {
                            "electl": {
                                //喷液吸液电机上位置
                                "id": 1,
                                "ele": "dp8_suck",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        },
                        "ontip_down": {
                            "electl": {
                                //上tip头z轴下位置
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        },
                        "ontip_up": {
                            "electl": {
                                //上tip头z轴上位置
                                "id": 1,
                                "ele": "dp8_z",
                                "speed": 0,
                                "acc": 0,
                                "dec": 0,
                                "position": 0
                            }
                        }
                    }
                },

            },
        num: 0,


    },
    mounted: function () {
        this.getConfig();
    },
    methods: {
        getConfig: function () {
            let _s = this;

            if (_s.num < Object.keys(_s.config).length) {
                axios.post(_s.url + "/v1/param/to_pc", {
                    "fileId": Number(_s.num)
                })
                    .then(function (res) {
                        if (res.data.length != 0) {
                            _s.ConfigProcess(res.data, _s.num);
                        }
                        _s.num++;
                        _s.getConfig();
                    })
            } else {
                console.log(_s.config);
            }

        },
        ConfigProcess: function (ConfigFile, k) {

            var _s = this;
            if (typeof (ConfigFile) == "string") {
                var b = [];
                var result = "";
                var a = ConfigFile.split("{\"result\":");
                var j = 0;
                for (var i = 1; i < a.length; i++) {
                    a[i] = a[i].substring(0, a[i].length - 2);
                    j = i - 1;
                    b[j] = JSON.parse(a[i]);
                }
                for (var i = 0; i < b.length; i++) {
                    result = result + b[i].chunk;
                }
                result = JSON.parse(result);
            }else if(typeof (ConfigFile) == "object"){
               result=JSON.parse(ConfigFile.result.chunk)
            }
            switch (k) {
                case 0:
                    _s.config.OpenLid = result;
            }
            switch (k) {
                case 0:
                    _s.config.OpenLid = result;
                    break;
                case 1:
                    _s.config.CloseLid = result;
                    break;
                case 2:
                    _s.config.xr = result;
                    break;
                case 3:
                    _s.config.Bash = result;
                    break;
                case 4:
                    _s.config.Trans = result;
                    break;
                case 5:
                    _s.config.dp8 = result;
                    break;
                case 6:
                    _s.config.Index = result;
                    break;
                default:
                    alert("配置文件处理出错");
                    break;
            }
            console.log(result, k);
        },


        saveConfig: function (Filesize, str,num) {
            let _s = this;
            axios.post(_s.url + "/v1/param/to_board", {
                "fileId": Number(num),
                "filesize": Filesize,
                "chunk": str
            })
                .then(function (res) {
                    console.log(res)
                })
        },
        saveConfigProcess: function (num) {
            let _s = this;
            var str = JSON.stringify(_s.config.dp8);
            console.log(str, str.length);
            _s.saveConfig(str.length, str,num)
        },


    }


});