package main

import (
	pb "another/pb"
	"context"
	"flag"
	"google.golang.org/grpc"
	"log"
)

const (
	authority    = "a"
)

//手写模拟客户端
func main() {
	log.SetFlags(log.Ltime|log.Lshortfile)
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	// WithAuthority会将传入值设置到请求头的:authority字段中,服务端可以检查此字段(如果未用安全套接字,此字段将以明文进行传输)
	opts = append(opts,
		grpc.WithAuthority(authority),
		grpc.WithStreamInterceptor(StreamClientInterceptor),
		grpc.WithUnaryInterceptor(UnaryClientInterceptor),
	)


	//可以使用 ./clinet -p xxx 指定监听地址为xxx，不指定则默认为：8787
	target := flag.String("p", "127.0.0.1:8787", "server address(ip:port)")
	flag.Parse()
	conn, err := grpc.Dial(*target, opts...)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()
	if conn == nil {
		panic("conn is nil")
	}
	//创建客户端实例
	clientWeather := pb.NewWeatherClient(conn)

	//发起请求1
	res, err := clientWeather.QueryCity(context.Background(), &pb.QueryType{
		Name:                 "City you want to query",
		Code:                 233,
	})
	if err != nil {
		panic(err)
	}
	log.Println("1:", res)

	//发起请求2
	res, err = clientWeather.QueryRegion(context.Background(), &pb.QueryType{
		Name:                 "Region you want to query",
		Code:                 233,
	})
	if err != nil {
		panic(err)
	}
	log.Println("2:", res)

}

