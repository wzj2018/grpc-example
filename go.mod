module another

go 1.14

require (
	github.com/gogo/protobuf v1.3.1
	github.com/golang/protobuf v1.4.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.0
	github.com/grpc-ecosystem/grpc-gateway v1.14.5
	golang.org/x/net v0.0.0-20200513185701-a91f0712d120
	google.golang.org/genproto v0.0.0-20200514193133-8feb7f20f2a2
	google.golang.org/grpc v1.29.1
)
