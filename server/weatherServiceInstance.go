package main

import (
	"another/pb"
	"context"
	"math/rand"
)

//随便实现一个最简的server service
type WeatherServer struct {

}

//rpc1模拟内容
func (w *WeatherServer) QueryRegion(ctx context.Context, queryType *weather.QueryType) (*weather.ResponseType, error) {
	return &weather.ResponseType{
		Result: "Great weather, The REGION you're querying:" + queryType.Name,
		Status: rand.Uint32(),
	}, nil
}

//rpc2模拟内容
func (w *WeatherServer) QueryCity(ctx context.Context, queryType *weather.QueryType) (*weather.ResponseType, error) {
	return &weather.ResponseType{
		Result: "Great weather, although I just made it up! The city you're querying:" + queryType.Name,
		Status: rand.Uint32(),
	}, nil
}

