//go:generate protoc --go_out=plugins=grpc:../gp -I ../gp ../gp/weather.proto

package main

import (
	smart "another/pb"
	"context"
	"flag"
	"fmt"
	go_grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"log"
	"net"
)

const Token = "a"
const Authority = "a"

func main() {
	port := flag.Int("p", 8787, "listen port")
	flag.Parse()
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	// 新建grpc服务,并传入拦截器进行认证检查
	grpcServer := grpc.NewServer(
		grpc.StreamInterceptor(go_grpc_middleware.ChainStreamServer(
			grpc_auth.StreamServerInterceptor(auth),
		)),
		grpc.UnaryInterceptor(go_grpc_middleware.ChainUnaryServer(
			grpc_auth.UnaryServerInterceptor(auth),
		)),
	)

	//scannerIns := &ScannerServiceInstance{}
	//sealerIns := &SealerInstance{}
	//pcrIns := &PCRInstance{}
	weatherInstance := &WeatherServer{}

	// 注册grpc实例
	//smart.RegisterScannerServiceServer(grpcServer, scannerIns)
	//smart.RegisterSealerServiceServer(grpcServer, sealerIns)
	//smart.RegisterMetalBathServiceServer(grpcServer, pcrIns)
	smart.RegisterWeatherServer(grpcServer, weatherInstance)

	log.Printf("server start at %v", *port)
	_ = grpcServer.Serve(lis)
}

// auth 对传入的metadata中的:authority内容进行判断,失败后再对token进行判断
func auth(ctx context.Context) (ctx1 context.Context, err error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		err = status.Error(codes.Unauthenticated, "token信息不存在")
		return
	}
	if authority, exists := md[":authority"]; exists && len(authority) > 0 {
		if authority[0] == Authority {
			ctx1 = ctx
			return
		}
	}
	tokenMD := md["token"]
	if tokenMD == nil || len(tokenMD) < 1 {
		err = status.Error(codes.Unauthenticated, "token信息不存在")
		return
	}
	if err = tokenValidate(tokenMD[0]); err != nil {
		err = status.Error(codes.Unauthenticated, err.Error())
		return
	}
	ctx1 = ctx
	return
}

func tokenValidate(token string) (err error) {
	if token != Token {
		err = fmt.Errorf("token认证错误")
		return
	}
	return
}
